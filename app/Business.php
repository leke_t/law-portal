<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Business extends Model
{
    protected $fillable = ['user_id', 'avatar', 'company_name', 'service', 'profile'];

    public function user()
	{
		return $this->belongsTo('App\User');
	}
}
