<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\User;
use App\Due;
use Carbon\Carbon;

class CreateDues extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'nbaportal:dues';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create dues record in the db for every user.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
       $users = User::getAllUsers();
        $now = Carbon::now();

        for($i = 0; $i < count($users); $i++){
            $match = ['user_id' => $users[$i]->id, 'year' => $now->year];
            $due = Due::where($match)->first();
            if($due == null){
                $dues = new Due;
                $dues->user_id = $users[$i]->id;
                $dues->amount = 5000.00;
                $dues->year = $now->year;
                $dues->paid = Due::UNPAID;
                $dues->purpose = 'Payment for ' . $now->year . ' Annual Membership Fee';
                $dues->save();
            }
        }
    }
}
