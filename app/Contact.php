<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    protected $fillable = ['user_id', 'phone', 'alt_phone', 'alt_email', 'address'];

    public function user()
	{
		return $this->belongsTo('App\User');
	}
}
