<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Due extends Model
{
    //
    protected $fillable = ['user_id', 'amount', 'year', 'paid'];

    const PAID = '1';
    const UNPAID = '0';

    public function user()
	{
		return $this->belongsTo('App\User');
	}
}
