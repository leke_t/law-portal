<?php

namespace App\Http\Controllers\Dues;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;
use App\Mail\Receipt;
use App\User;
use App\Due;
use App\Personal;
use Illuminate\Support\Facades\DB;
use Auth;
use Paystack;
use PDF;


class DuesController extends Controller
{
    //
    public function index()
    {
    	$user = Auth::user();
    	$dues = Due::where('user_id', $user->id)->get();
    	$pid = DB::table('personals')->where('user_id', $user->id)->first();

        $data['avatar'] = $pid->avatar;
        $data['dues'] = $dues;
    	$data['fullname'] = $user->first_name . ' ' . $user->last_name;
    	$data['email'] = $user->email;

    	return view('pages.dues.history', array('user' => $data));
    }

    /**
     * Redirect the User to Paystack Payment Page
     * @return Url
     */
    public function redirectToGateway()
    {
        return Paystack::getAuthorizationUrl()->redirectNow();
    }

    /**
     * Obtain Paystack payment information
     * @return void
     */
    public function handleGatewayCallback()
    {
        $paymentDetails = Paystack::getPaymentData();

        //dd($paymentDetails);
        if($paymentDetails['message'] == 'Verification successful'){
        	if($paymentDetails['data']['gateway_response'] == 'Successful'){
	        	$user = Auth::user();
	    		$dues = Due::find($paymentDetails['data']['metadata']['dues_id']);

	    		$dues->paid = Due::PAID;
	    		$dues->reference = $paymentDetails['data']['reference'];
	    		$dues->save();

                Mail::to($user->email)->send(new Receipt($paymentDetails['data']['metadata']['dues_id']));

	    		return redirect()->route('payment.success');
        	}	
        }
        else
        {
        	return redirect()->route('payment.error');
        }
    }

    public function paymentSuccessPage()
    {
    	$user = Auth::user();
    	$pid = DB::table('personals')->where('user_id', $user->id)->first();

        $data['avatar'] = $pid->avatar;
    	$data['fullname'] = $user->first_name . ' ' . $user->last_name;

    	return view('pages.dues.success',  array('user' => $data));
    }

    public function receipt(Request $request)
    {
    	$dues = Due::find($request->input('id'));
        $user = Auth::user();

        if($request->has('send')){
        	 Mail::to($user->email)->send(new Receipt($dues->id));
        }

        return redirect()->back();
    }
}