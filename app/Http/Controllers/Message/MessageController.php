<?php

namespace App\Http\Controllers\Message;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Message;
use App\User;
use App\Personal;
use Auth;
use Illuminate\Support\Facades\DB;
use Validator;

class MessageController extends Controller
{

	public function getComposeBox($id)
    {
        $data['id'] = $id;

        $user = Auth::user();
        $pid = DB::table('personals')->where('user_id', $user->id)->first();
        $data['fullname'] = $user->first_name . ' ' . $user->last_name;
        $data['first_name'] = $user->first_name;
        $data['last_name'] = $user->last_name;
        $data['avatar'] = $pid->avatar;

        return view('pages.website.message-centre')->with('recipient', $data)->with('user', $data);
    }

    public function sendMessage(Request $request)
    {
        $receiver_id = $_POST['recipient'];

        $validator = Validator::make($request->all(), [
            'message' => 'required'
        ]);

        if ($validator->fails()) {
            session()->flash('message', 'One or more error has occured.');
            return redirect()->back();
        }
        $user = Auth::user();
        $recipient = User::find($receiver_id);
        $message = new Message();

        try {
            $message->sender_id = $user->id;
            $message->sender = $user->first_name . ' ' . $user->last_name;
            $message->recipient_id = $receiver_id;
            $message->recipient = $recipient->first_name . ' ' . $recipient->last_name;
            $message->message = $request->input('message');

            $message->save();

            session()->flash('message', 'Your message has been sent successfuly');

            return redirect()->route('message.inbox');

        } catch (Exception $e) {
            session()->flash('message', 'Caught exception: ' .  $e->getMessage());
        }

    }

    public function inbox()
    {
        $user = Auth::user();
        $pid = DB::table('personals')->where('user_id', $user->id)->first();
        $data['fullname'] = $user->first_name . ' ' . $user->last_name;
        $data['first_name'] = $user->first_name;
        $data['last_name'] = $user->last_name;
        $data['avatar'] = $pid->avatar;

        $messages = Message::where('recipient_id', $user->id)
                            ->paginate(2);

        $data['personals'] = [];

        foreach ($messages as $message){
             $sender = Personal::where('user_id', $message->sender_id)->first();
             array_push($data['personals'], $sender);
         } 

        return view('pages.website.inbox')->with('messages', $messages)->with('user', $data);
    }

     public function outbox()
    {
        $user = Auth::user();
        $pid = DB::table('personals')->where('user_id', $user->id)->first();
        $data['fullname'] = $user->first_name . ' ' . $user->last_name;
        $data['first_name'] = $user->first_name;
        $data['last_name'] = $user->last_name;
        $data['avatar'] = $pid->avatar;

        $messages = Message::where('sender_id', $user->id)
                            ->paginate(2);
        
        $data['personals'] = [];

        foreach ($messages as $message){
             $recipient = Personal::where('user_id', $message->recipient_id)->first();
             array_push($data['personals'], $recipient);
        } 

        return view('pages.website.outbox')->with('messages', $messages)->with('user', $data);
    }
    
}
