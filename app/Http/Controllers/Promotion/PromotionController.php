<?php

namespace App\Http\Controllers\Promotion;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Personal;
use App\Promotions;
use App\PromotionalServices;
use App\Mail\Promotion;
use Auth;
use Illuminate\Support\Facades\DB;
use Validator;
use Illuminate\Support\Facades\Mail;

class PromotionController extends Controller
{
    public function index()
    {

        $user = Auth::user();
        $pid = DB::table('personals')->where('user_id', $user->id)->first();
        $data['fullname'] = $user->first_name . ' ' . $user->last_name;
        $data['first_name'] = $user->first_name;
        $data['last_name'] = $user->last_name;
        $data['avatar'] = $pid->avatar;

    	return view('pages.website.promote')->with('user', $data);
    }

    public function promote(Request $request)
    {
    	$user = Auth::user();
    	$result = $request->all();

        //dd($result);

    	$promotion = new Promotions();

    	$promotion->user_id = $user->id;
    	$promotion->company_name = $request->input('company_name');
    	$promotion->description = $request->input('description');
    	$promotion->save();

    	foreach($result['check'] as $service)
    	{
    		$new_service = new PromotionalServices();
    		$new_service->promotion_id = $promotion->id;
    		$new_service->service = $service;
    		$new_service->save();
    	}

        Mail::to('admin@nbaportal.com')->send(new Promotion($promotion->id));

        session()->flash('message', 'You message has been sent successfuly');
    	return redirect()->back();
    }
}
