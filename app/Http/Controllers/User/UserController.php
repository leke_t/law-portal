<?php

namespace App\Http\Controllers\User;

use App\Achievement as Achievement;
use App\Business as Business;
use App\Contact as Contact;
use App\Education as Education;
use App\Http\Controllers\Controller;
use App\Mail\ExistingMemberSignup as ExistingMember;
use App\Personal as Personal;
use App\TrainingAndCertification as Training;
use App\User;
use App\Mail\PasswordReset;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Validator;
use Auth;


class UserController extends Controller
{
     /**
     *
     * @return the website's home page
     */
    public function index()
    {
        return view('pages.website.home');
    }

    /**
     *
     * @return Loging page
     */
    public function loginView()
    {
        return view('pages.users.login');
    }

    /**
     *
     * @return Signup page for new members
     */
    public function newMember()
    {
    	return view('pages.users.new-member');
    }

    /**
     * Saves new member
     * @return Page to enter personal infomation
     */
   public function newMemberStore(Request $request)
   {
   		
        $this->validate(request(), [
        'first_name' => 'required',
         'last_name' => 'required',
         'email' => 'required|email|unique:users',
         'phone' => 'required|min:11|numeric',
         'password' => 'required|min:6',
     ]);

         $user = new User();

         $user->first_name = $request->input('first_name');
         $user->last_name = $request->input('last_name');
         $user->email = $request->input('email');
         $user->password = bcrypt($request->input('password'));
         $user->membership_id = $user::generateMembershipID();
         $user->verification_token = $user::generateVerificationToken();
         $user->category = $user::REGULAR_USER;

         $user->save();

         $contact = new Contact(['user_id' => $user->id, 'phone' => $request->input('phone')]);
         $contact->save();

         $personal = new Personal(['user_id' => $user->id]);
         $personal->save();

         Auth::login($user);
         
         return redirect()->route('personal.info');

   }

    public function existingMember()
    {
    	return view('pages.users.existing-member');
    }

    public function existingMemberStore(Request $request)
    {
   		$validator = Validator::make($request->all(), [
    		'memberid' => 'required',
    	]);

    	 if ($validator->fails()) {
               	session()->flash('message', 'One or more error has occured.');
                return redirect()->back()->withInput();
         }

         $user = DB::table('users')->where('membership_id', $request->input('memberid'))->first();
         $token = $user->verification_token;

         Mail::to($user->email)->send(new ExistingMember($token));

         //dd($user['7']->verification_token);
        
         return redirect()->route('login');

    }

    public function setExistingMemberPassword($token)
    {
    	$returnedToken['token'] = $token;

    	return view('pages.users.set-existing-member-password')->with('token', $returnedToken);;
    }

    public function setPassword(Request $request)
    {

    	$verification_token = $_POST['token'];

    	$validator = Validator::make($request->all(), [
    		'password' => 'required|min:6|confirmed',
    	]);

    	 if ($validator->fails()) {
               	session()->flash('message', 'One or more error has occured.');
                return redirect()->back();
         }

        $reset = DB::table('users')->where('verification_token', $verification_token)->first();

        if(isset($reset[0])){
            $user = User::find($reset->id);

            $user->password = bcrypt($request->input('password'));
            $user->verified = User::VERIFIED_USER;
            $user->save();

            session()->flash('message', 'Password reset was successful.');
            session()->flash('alert', 'alert-success');

            return redirect()->route('login');
        }else
        {
            session()->flash('message', 'Verification token has expired');
            return redirect()->route('existingMember.signup');
        }
       
    }


    public function business()
    {
    	return view('pages.users.business-signup');
    }

   public function businessStore(Request $request)
   {
        $this->validate(request(), [
            'company' => 'required',
            'email' => 'required|email|unique:users',
            'memberid' => 'required',
            'password' => 'required|min:6',
        ]);

         $user = new User();

         $user->email = $request->input('email');
         $user->password = bcrypt($request->input('password'));
         $user->membership_id = $request->input('memberid');
         $user->verification_token = $user::generateVerificationToken();
         $user->category = $user::BUSINESS_USER;

         $user->save();
		
		 $business = new Business(['user_id' => $user->id, 'company_name' => $request->input('company')]);
         $business->save();
                 
         return redirect()->route('login');

   }

    public function login(Request $request)
    {
        $validator = Validator::make($request->all(), [
    		'email' => 'required|email',
    		'password' => 'required'
    	]);

    	 if ($validator->fails()) {
               	session()->flash('message', 'One or more error has occured.');
                return redirect()->back();
         }

         if(Auth::attempt([
         	'email' => $request->input('email'),
         	'password' => $request->input('password')
         ])){
         		return redirect()->route('dashboard');
         }else
         {
         	session()->flash('message', 'Email or password incorrect');
         	return redirect()->back();
         }
    }

    public function passwordResetView($token=null)
    {
    	if(isset($token)){
    		$returnedToken['token'] = $token;
    		return view('pages.users.password.set')->with('token', $returnedToken);
    	}
    	else
    	{
    		return view('pages.users.password.email');
    	}
    }

    public function sendResetEmail(Request $request)
    {
    	$validator = Validator::make($request->all(), [
    		'email' => 'required|email',
    	]);

    	 if ($validator->fails()) {
               	session()->flash('message', 'You need to enter a valid email');
                return redirect()->back();
         }

        $email = DB::table('users')->where('email', $request->input('email'))->get();

        if(isset($email[0])){
            $token = str_random(60);

            $user = User::find($email[0]->id);

            $user->remember_token = $token;

            $user->save();

            Mail::to($email['0']->email)->send(new PasswordReset($token));

            session()->flash('message', 'An confirmation link has been sent to ' . $email['0']->email) ;

            return redirect()->back();
        }else
        {
            session()->flash('message', 'This email does not exist');

            return redirect()->back();
        }
        
    }

    public function resetPassword(Request $request)
    {
    	$remember_token = $_POST['token'];

    	$validator = Validator::make($request->all(), [
    		'password' => 'required|min:6|confirmed',
    	]);

    	if($validator->fails()) {
            session()->flash('message', 'Password does not match');
            return redirect()->back();
        }

        $token = DB::table('users')->where('remember_token', $remember_token)->get();
        if(isset($token[0])){
                $user = User::find($token[0]->id);

            $user->password = bcrypt($request->input('password'));
            $user->save();

            return redirect()->route('user.login');
        }else{
            session()->flash('message', 'Reset token has expired');
            return redirect()->route('get.passwordResetViewWithoutToken');
        }
    

    }

    public function logout()
    {
        Auth::logout();
        return redirect()->route('user.login');
    }

}
