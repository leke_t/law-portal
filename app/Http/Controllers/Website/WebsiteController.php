<?php

namespace App\Http\Controllers\Website;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Achievement as Achievement;
use App\Business as Business;
use App\Contact as Contact;
use App\Education as Education;
use App\Personal as Personal;
use App\TrainingAndCertification as Training;
use App\User;
use App\Message;
use App\Mail\PasswordReset;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Validator;
use Auth;
use Image;

class WebsiteController extends Controller
{
    protected $user;

    public function index()
    {
    	$user = Auth::user();

        $pid = DB::table('personals')->where('user_id', $user->id)->first();
        $cid = DB::table('contacts')->where('user_id', $user->id)->first();

        $contact = Contact::find($user->id);
        $education = Education::where('user_id', $user->id)->get();
        $training = Training::where('user_id', $user->id)->get();
        $achievement = Achievement::where('user_id', $user->id)->get();
       

        $data['fullname'] = $user->first_name . ' ' . $user->last_name;
        $data['email'] = $user->email;

        $data['avatar'] = $pid->avatar;
        $data['gender'] = $pid->gender;
        $data['dob'] = $pid->dob;
        $data['state'] = $pid->state;
        $data['lga'] = $pid->local_government;

        $data['phone'] = $cid->phone;
        $data['address'] = $cid->address;

        $data['education'] = $education;

        $data['training'] = $training;

        $data['achievement'] = $achievement;

        return view('pages.website.profile')->with('user', $data);
    }

    public function setPersonalInformation()
    {
    	
    	$user = Auth::user();
        $pid = DB::table('personals')->where('user_id', $user->id)->first();
        $data['fullname'] = $user->first_name . ' ' . $user->last_name;
    	$data['first_name'] = $user->first_name;
    	$data['last_name'] = $user->last_name;
    	$data['id'] = $user->id;
        $data['avatar'] = $pid->avatar;

    	return view('pages.users.setup-wizard.step-1')->with('user', $data);
    }

    public function updatePersonalInformation()
    {
        $this->user = Auth::user();
        $pid = DB::table('personals')->where('user_id', $this->user->id)->first();

        $data['fullname'] = $this->user->first_name . ' ' . $this->user->last_name;
        $data['first_name'] = $this->user->first_name;
        $data['last_name'] = $this->user->last_name;
        $data['id'] = $this->user->id;
        $data['avatar'] = $pid->avatar;
        $data['gender'] = $pid->gender;
        $data['dob'] = $pid->dob;
        $data['state'] = $pid->state;
        $data['lga'] = $pid->local_government;

        return view('pages.users.setup-wizard.personal-info-update')->with('user', $data);
    }

    public function personalInformation(Request $request)
    {
    	$id = $_POST['user_id'];

    	$validator = Validator::make($request->all(), [
    		'gender' => 'required',
    		'dob' => 'required',
    		'state' => 'required',
    		'lga' => 'required',
    	]);

    	if ($validator->fails()) {
            session()->flash('message', 'One or more error has occured.');
            return redirect()->back()->withInput();
        }


        $user = DB::table('personals')->where('user_id', $id)->get();

        $personal = Personal::find($user[0]->id);

        if($request->hasFile('avatar')){
            $avatar = $request->file('avatar');
            $filename = time() . '.' . $avatar->getClientOriginalExtension();
            $path = public_path('/uploads/avatars/' . $filename);
            Image::make($avatar)->resize(50, 50)->save($path);
            $personal->avatar = $filename;
        }

        $personal->gender = $request->input('gender');
        $personal->dob = $request->input('dob');
        $personal->state = $request->input('state');
        $personal->local_government = $request->input('lga');

        $personal->save();

        return redirect()->route('contact.info');
    }

    public function updatePersonalInformationStore(Request $request)
    {
        $id = $_POST['user_id'];

        $user = DB::table('personals')->where('user_id', $id)->get();

        $personal = Personal::find($user[0]->id);

        if($request->hasFile('avatar')){
            $avatar = $request->file('avatar');
            $filename = time() . '.' . $avatar->getClientOriginalExtension();
            $path = public_path('/uploads/avatars/' . $filename);
            Image::make($avatar)->resize(47, 46)->save($path);
            $personal->avatar = $filename;
        }

        if($request->has('gender')){
            $personal->gender = $request->input('gender');
        }
       
        if($request->has('dob')){
            $personal->dob = $request->input('dob');
        }
        
        if($request->has('state')){
            $personal->state = $request->input('state');
        }
        
        if($request->has('lga')){
            $personal->local_government = $request->input('lga');
        }

        $personal->save();

        return redirect()->route('dashboard');
    }


     public function setContactInformation()
    {

    	$this->user = Auth::user();
        $pid = DB::table('personals')->where('user_id', $this->user->id)->first();
        $personal = Personal::find($pid->id);
        $cid = DB::table('contacts')->where('user_id', $this->user->id)->first();
        $contact = Contact::find($cid->id);
        

        $data['avatar'] = $personal->avatar;
       
        $data['fullname'] = $this->user->first_name . ' ' . $this->user->last_name;
    	$data['email'] = $this->user->email;
    	$data['phone'] = $contact->phone;
    	$data['id'] = $this->user->id;

    	return view('pages.users.setup-wizard.step-2')->with('user', $data);
    }

    public function updateContactInformation()
    {

        $this->user = Auth::user();
        $id = DB::table('personals')->where('user_id', $this->user->id)->get();
        $personal = Personal::find($id[0]->id);
        $contact = Contact::find($id[0]->id);

        $data['avatar'] = $personal->avatar;
       
        $data['fullname'] = $this->user->first_name . ' ' . $this->user->last_name;
        $data['email'] = $this->user->email;
        $data['id'] = $this->user->id;
        $data['phone'] = $contact->phone;
        $data['alt_email'] = $contact->alt_email;
        $data['alt_phone'] = $contact->alt_phone;
        $data['address'] = $contact->aadress;

        return view('pages.users.setup-wizard.contact-info-update')->with('user', $data);
    }

    public function contactInformation(Request $request)
    {
    	$id = $_POST['user_id'];

    	$validator = Validator::make($request->all(), [
            'address' => 'required'
    	]);

    	if ($validator->fails()) {
            session()->flash('message', 'You need to enter your address');
            return redirect()->back();
        }

        $user = DB::table('contacts')->where('user_id', $id)->get();

        $contact = Contact::find($user[0]->id);

        $contact->alt_email = $request->input('alt_email');
        $contact->phone = $request->input('phone');
        $contact->alt_phone = $request->input('alt_phone');
        $contact->address = $request->input('address');

        $contact->save();

        return redirect()->route('set.education');
    }

    public function updateContactInformationStore(Request $request)
    {
        $id = $_POST['user_id'];

        $user = DB::table('contacts')->where('user_id', $id)->get();

        $contact = Contact::find($user[0]->id);

        if($request->has('alt_email'))
        {
            $contact->alt_email = $request->input('alt_email');
        }

        if($request->has('phone'))
        {
            $contact->phone = $request->input('phone');
        }

        if($request->has('alt_phone'))
        {
            $contact->alt_phone = $request->input('alt_phone');
        }

        if($request->has('address'))
        {
            $contact->address = $request->input('address');
        }

        $contact->save();

        return redirect()->route('dashboard');
    }

    public function setEducation()
    {

        $this->user = Auth::user();
       
        $id = DB::table('personals')->where('user_id', $this->user->id)->get();
        $personal = Personal::find($id[0]->id);
        $contact = Contact::find($id[0]->id);

        $data['avatar'] = $personal->avatar;
        $data['fullname'] = $this->user->first_name . ' ' . $this->user->last_name;
        $data['id'] = $this->user->id;
    
        return view('pages.users.setup-wizard.step-3')->with('user', $data);
    }

      public function updateEducation(Request $request)
    {
        if($request->has('add')){
            $this->user = Auth::user();
            $id = DB::table('personals')->where('user_id', $this->user->id)->get();
            $personal = Personal::find($id[0]->id);
            $contact = Contact::find($id[0]->id);
            $data['avatar'] = $personal->avatar;
            $data['fullname'] = $this->user->first_name . ' ' . $this->user->last_name;
            $data['id'] = $this->user->id;

            return view('pages.users.setup-wizard.education')->with('user', $data);

        }else{
            $this->user = Auth::user();
            $id = DB::table('personals')->where('user_id', $this->user->id)->get();
            $personal = Personal::find($id[0]->id);
            $contact = Contact::find($id[0]->id);

            $education = Education::where('user_id', $this->user->id)->get();

            $data['avatar'] = $personal->avatar;
            $data['fullname'] = $this->user->first_name . ' ' . $this->user->last_name;
            $data['id'] = $this->user->id;
            $data['education'] = $education;

            return view('pages.users.setup-wizard.education-update')->with('user', $data);
        }
    }

    public function education(Request $request)
    {
        $id = $_POST['user_id'];
        $result = $request->all();

        if($result['certificate'][0] != null){
            for($i = 0; $i < count($result['certificate']); $i++){
                $education = new Education();
                $education->user_id = $id;
                $education->certificate = $result['certificate'][$i];
                $education->institution = $result['institution'][$i];
                $education->year = $result['year'][$i];

                $education->save();
            }
        }
        return redirect()->route('set.training');

    }

     public function updateEducationStore(Request $request)
    {
        $user = Auth::user();
        $id = $_POST['user_id'];
        $result = $request->all();
        $educations = Education::where('user_id', $user->id)->get();
        
        //When adding from the profile page after the user skipped it the first time
        if($request->has('add')){
            for($i = 0; $i < count($result['certificate']); $i++){
                $education = new Education();
                $education->user_id = $id;
                $education->certificate = $result['certificate'][$i];
                $education->institution = $result['institution'][$i];
                $education->year = $result['year'][$i];

                $education->save();
            }
  
            return redirect()->route('dashboard');
        }

        // If the use added new records
        if(count($result['certificate']) > count($educations))
        {
            for($i = 0; $i < count($result['certificate']); $i++){
                //Adds the new records
                if($i > (count($educations) - 1)){
                    $education = new Education();
                    $education->user_id = $id;
                    $education->certificate = $result['certificate'][$i];
                    $education->institution = $result['institution'][$i];
                    $education->year = $result['year'][$i];

                    $education->save();
                }
                //Edits existing records
                else{
                    $education = Education::find($_POST['id'][$i]);
                    $education->certificate = $result['certificate'][$i];
                    $education->institution = $result['institution'][$i];
                    $education->year = $result['year'][$i];

                    $education->save();
                }
             }
        }else{
            for($i = 0; $i < count($educations); $i++){
                $education = Education::find($_POST['id'][$i]);
                $education->certificate = $result['certificate'][$i];
                $education->institution = $result['institution'][$i];
                $education->year = $result['year'][$i];

                $education->save();
            }
        }
  
        return redirect()->route('dashboard');

    }


    public function setTrainingAndCertifications()
    {

        $this->user = Auth::user();
        $id = DB::table('personals')->where('user_id', $this->user->id)->get();
        $personal = Personal::find($id[0]->id);
        $contact = Contact::find($id[0]->id);

        $data['avatar'] = $personal->avatar;
        $data['id'] = $this->user->id;

        $data['fullname'] = $this->user->first_name . ' ' . $this->user->last_name;

        return view('pages.users.setup-wizard.step-4')->with('user', $data);
    }

     public function updateTrainingAndCertifications(Request $request)
    {

        $this->user = Auth::user();
        $id = DB::table('personals')->where('user_id', $this->user->id)->get();
        $personal = Personal::find($id[0]->id);
        $contact = Contact::find($id[0]->id);
        $training = Training::where('user_id', $this->user->id)->get();
       
        if($request->has('add')){
            $data['avatar'] = $personal->avatar;
            $data['id'] = $this->user->id;

            $data['fullname'] = $this->user->first_name . ' ' . $this->user->last_name;

            return view('pages.users.setup-wizard.training')->with('user', $data);
        }

        $data['avatar'] = $personal->avatar;
        $data['id'] = $this->user->id;
        $data['training'] = $training;

        $data['fullname'] = $this->user->first_name . ' ' . $this->user->last_name;

        return view('pages.users.setup-wizard.training-update')->with('user', $data);
    }

    public function trainingAndCertifications(Request $request)
    {
        $id = $_POST['user_id'];
        $result = $request->all();

        if($result['certificate'][0] != null){
            for($i = 0; $i < count($result['certificate']); $i++){
                $training = new Training();
                $training->user_id = $id;
                $training->certificate = $result['certificate'][$i];
                $training->institution = $result['institution'][$i];
                $training->year = $result['year'][$i];

                $training->save();
            }
        }

        return redirect()->route('set.achievement');

    }

     public function updateTrainingAndCertificationsStore(Request $request)
    {
        $user = Auth::user();
        $id = $_POST['user_id'];
        $result = $request->all();
        $trainings = Training::where('user_id', $user->id)->get();
        
        if($request->has('add')){
            for($i = 0; $i < count($result['certificate']); $i++){
                $training = new Training();
                $training->user_id = $id;
                $training->certificate = $result['certificate'][$i];
                $training->institution = $result['institution'][$i];
                $training->year = $result['year'][$i];

                $training->save();
            }
        
            return redirect()->route('dashboard');
        }

        if(count($result['certificate']) > count($trainings))
        {
            for($i = 0; $i < count($result['certificate']); $i++){
                if($i > (count($trainings) - 1)){
                    $training = new Training();
                    $training->user_id = $id;
                    $training->certificate = $result['certificate'][$i];
                    $training->institution = $result['institution'][$i];
                    $training->year = $result['year'][$i];

                    $training->save();
                }else{
                    $training = Training::find($_POST['id'][$i]);
                    $training->certificate = $result['certificate'][$i];
                    $training->institution = $result['institution'][$i];
                    $training->year = $result['year'][$i];

                    $training->save();
                }
             }
        }else{
            for($i = 0; $i < count($trainings); $i++){
                $training = Training::find($_POST['id'][$i]);
                $training->certificate = $result['certificate'][$i];
                $training->institution = $result['institution'][$i];
                $training->year = $result['year'][$i];

                $training->save();
            }
        }
  
        return redirect()->route('dashboard');
    }


    public function setAchievements()
    {

        $this->user = Auth::user();
        $id = DB::table('personals')->where('user_id', $this->user->id)->get();
        $personal = Personal::find($id[0]->id);
        $contact = Contact::find($id[0]->id);
    

        $data['avatar'] = $personal->avatar;
        $data['id'] = $this->user->id;
        $data['fullname'] = $this->user->first_name . ' ' . $this->user->last_name;

        return view('pages.users.setup-wizard.step-5')->with('user', $data);
    }

    public function updateAchievements(Request $request)
    {

        $this->user = Auth::user();
        $id = DB::table('personals')->where('user_id', $this->user->id)->get();
        $personal = Personal::find($id[0]->id);
        $contact = Contact::find($id[0]->id);
        $achievement = Achievement::where('user_id', $this->user->id)->get();
    
        if($request->has('add')){
            $data['avatar'] = $personal->avatar;
            $data['id'] = $this->user->id;
            $data['fullname'] = $this->user->first_name . ' ' . $this->user->last_name;

            return view('pages.users.setup-wizard.achievement')->with('user', $data);
        }else{
            $data['avatar'] = $personal->avatar;
            $data['id'] = $this->user->id;
            $data['fullname'] = $this->user->first_name . ' ' . $this->user->last_name;
            $data['achievement'] = $achievement;

            return view('pages.users.setup-wizard.achievement-update')->with('user', $data);
        }
        
    }

    public function achievements(Request $request)
    {
        $id = $_POST['user_id'];
        $result = $request->all();
        if($result['project'][0] != null){
            for($i = 0; $i < count($result['project']); $i++){
                $education = new Achievement();
                $education->user_id = $id;
                $education->project = $result['project'][$i];
                $education->year = $result['year'][$i];

                $education->save();
            }
        }
        
        return redirect()->route('dashboard');

    }

     public function updateAchievementsStore(Request $request)
    {
        $user = Auth::user();
        $id = $_POST['user_id'];
        $result = $request->all();
        $achievements = Achievement::where('user_id', $user->id)->get();
        
        if($request->has('add')){

        for($i = 0; $i < count($result['project']); $i++){
            $achievement = new Achievement();
            $achievement->user_id = $id;
            $achievement->project = $result['project'][$i];
            $achievement->year = $result['year'][$i];

            $achievement->save();
        }

        
            return redirect()->route('dashboard');
        }

        if(count($result['project']) > count($achievements))
        {
            for($i = 0; $i < count($result['project']); $i++){
                if ($i > (count($achievements) - 1)) {
                    $achievement = new Achievement();
                    $achievement->user_id = $id;
                    $achievement->project = $result['project'][$i];
                    $achievement->year = $result['year'][$i];

                    $achievement->save();
                }else{
                    $achievement = Achievement::find($_POST['id'][$i]);
                    $achievement->project = $result['project'][$i];
                    $achievement->year = $result['year'][$i];

                    $achievement->save();
                }
             }

        }else{
            for($i = 0; $i < count($achievements); $i++){
                $achievement = Achievement::find($_POST['id'][$i]);
                $achievement->project = $result['project'][$i];
                $achievement->year = $result['year'][$i];

                $achievement->save();
            }
        }
  
        return redirect()->route('dashboard');

    }


     public function setBusinessInformation()
    {

        $this->user = Auth::user();
        $id = DB::table('personals')->where('user_id', $this->user->id)->get();
        $personal = Personal::find($id[0]->id);
        $contact = Contact::find($id[0]->id);

        $data['avatar'] = $personal->avatar;
        $data['fullname'] = $this->user->first_name . ' ' . $this->user->last_name;
        $company = DB::table('businesses')->where('user_id', $this->user->user_id)->get();

        if(isset($company[0])){
             $data['company_name'] = $company[0]->company_name;
            
        }else{
            $data['company_name'] = '';
        }  

        $data['id'] = $this->user->id;


        return view('pages.users.setup-wizard.business')->with('user', $data);
    }

    public function businessInformation(Request $request)
    {
        $id = $_POST['user_id'];

        $validator = Validator::make($request->all(), [
            'services' => 'required',
            'business_profile' => 'required'
        ]);

        if ($validator->fails()) {
            session()->flash('message', 'One or more error has occured.');
            return redirect()->back();
        }

        $user = DB::table('businesses')->where('user_id', $id)->get();

        $contact = Business::find($user[0]->id);

        $contact->service = $request->input('services');
        $contact->profile = $request->input('business_profile');

        $contact->save();

        return redirect()->route('set.training');

    }

    public function directory()
    {
        $user = Auth::user();
        $pid = DB::table('personals')->where('user_id', $user->id)->first();

        $data['avatar'] = $pid->avatar;
        $data['fullname'] = $user->first_name . ' ' . $user->last_name;

        return view('pages.website.search', array('user' => $data));
    }

     public function directoryResult(Request $request)
    {

        $user = Auth::user();
        $pid = DB::table('personals')->where('user_id', $user->id)->first();

        $data['avatar'] = $pid->avatar;
        $data['fullname'] = $user->first_name . ' ' . $user->last_name;

        $query = explode(' ', $request->input('query'));

        $array['query'] = $request->input('query');

        if(count($query) > 1){
             $users = User::where('first_name', $query[0])
                            ->orWhere('last_name', $query[0])
                            ->orWhere('first_name', $query[1])
                            ->orWhere('last_name', $query[1])
                            ->get();
        }else{
             $users = User::where('first_name', $query[0])
                             ->orWhere('last_name', $query[0])
                             ->get();
        }
        $array['users'] = $users;

        $array['personals'] = [];
        $array['education'] = [];

        foreach ($users as $user) {
            $pid = Personal::where('user_id', $user->id)->first();
            $education = Education::where('user_id', $user->id)->first();
            array_push($array['personals'], $pid);
            array_push($array['education'], $education);
        }


        $business = Business::where('company_name', 'LIKE', '%'.$query[0].'%')->get();
        $array['business'] = $business;
        //dd($array);

        //dd($array);

        $array['count'] = count($users) + count($business);
        
        return view('pages.website.search')->with('data', $array)->with('user', $data);
    }

    public function showUserProfile($id)
    {
        $auth_user = Auth::user();
        $user = User::find($id);

        if($auth_user->id == $id){
            return redirect()->route('dashboard');
        }

        if($user->category == 'individual'){
            $pid = DB::table('personals')->where('user_id', $user->id)->first();
            $auth_pid = DB::table('personals')->where('user_id', $auth_user->id)->first();
            $cid = DB::table('contacts')->where('user_id', $user->id)->first();
            $education = Education::where('user_id', $user->id)->get();
            $training = Training::where('user_id', $user->id)->get();
            $achievement = Achievement::where('user_id', $user->id)->get();
            
            $data['fullname'] = $auth_user->first_name . ' ' . $auth_user->last_name;
            $data['name'] = $user->first_name . ' ' . $user->last_name;
            $data['email'] = $user->email;
            $data['id'] = $user->id;

            $data['gender'] = $pid->gender;
            $data['dob'] = $pid->dob;
            $data['state'] = $pid->state;
            $data['lga'] = $pid->local_government;
            $data['avatar'] = $auth_pid->avatar;

            $data['phone'] = $cid->phone;
            $data['address'] = $cid->address;

            $data['education'] = $education;

            $data['training'] = $training;

            $data['achievement'] = $achievement;

            return view('pages.website.show-user-profile')->with('user', $data);

        }else{
            try {
                $business = Business::find($id);
                $achievement = Achievement::where('user_id', $user->id)->get();
                
                $data['fullname'] = $auth_user->first_name . ' ' . $auth_user->last_name;
                $data['email'] = $user->email;
                $data['id'] = $user->id;
                $pid = DB::table('personals')->where('user_id', $auth_user->id)->first();
                $data['avatar'] = $pid->avatar;

                $data['achievement'] = $achievement;

                return view('pages.website.show-business-profile')->with('user', $data);
            } catch (Exception $e) {
                session()->flash('message', 'Caught exception: ' .  $e->getMessage());
                return redirect()->back();
            }
        }
        
    }

     public function showBusinessProfile($id)
    {
        return view('pages.website.show-business-profile');
    }
}
