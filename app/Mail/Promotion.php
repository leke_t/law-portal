<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Promotions;
use App\PromotionalServices;

class Promotion extends Mailable
{
    use Queueable, SerializesModels;

    public $data;
    public $service;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($id)
    {
        
        $promotion = Promotions::find($id);
        $services = PromotionalServices::where('promotion_id', $id)->get();

        $this->data = $promotion;
        $this->service = $services;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.promotion');
    }
}
