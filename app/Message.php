<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    //
    protected $fillable = ['sender_id', 'recipient_id', 'subject', 'message'];

    public function user()
	{
		return $this->belongsTo('App\User');
	}
}
