<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Personal extends Model
{
    protected $fillable = ['user_id', 'avatar', 'gender', 'dob', 'state', 'local_government'];

    public function user()
	{
		return $this->belongsTo('App\User');
	}
}
