<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PromotionalServices extends Model
{
    protected $fillable = ['promotion_id', 'service'];

    public function promotion()
	{
		return $this->belongsTo('App\Promotions');
	}
}
