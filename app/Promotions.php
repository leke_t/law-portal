<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Promotions extends Model
{
    protected $fillable = ['user_id', 'company_name', 'service', 'description'];

    public function user()
	{
		return $this->belongsTo('App\User');
	}

	public function services()
	{
		return $this->hasMany('App\PromotionalServices');
	}
}
