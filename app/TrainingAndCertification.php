<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TrainingAndCertification extends Model
{
    protected $fillable = ['user_id', 'certificate', 'institution', 'year'];

    public function user()
	{
		return $this->belongsTo('App\User');
	}
}
