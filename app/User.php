<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Carbon\Carbon;

class User extends Authenticatable
{
    
    const UNVERIFIED_USER = '0';
    const VERIFIED_USER = '1';

    const REGULAR_USER = 'individual';
    const BUSINESS_USER = 'business';

    protected $table = 'users';


    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name', 'last_name', 'email', 'password', 'verified', 'verification_token', 'category',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', 'verification_token',
    ];

    // Relationship to the Achievement model
    public function achievement()
    {
          return $this->hasMany('App\Achievement');
    }

    // Relationship to the Business model
    public function business()
    {
          return $this->hasOne('App\Business');
    }
    
    // Relationship to the Personal model
    public function personal()
    {
          return $this->hasOne('App\Personal');
    }

    // Relationship to the Contact model
    public function contact()
    {
          return $this->hasOne('App\Contact');
    }

    // Relationship to the Education model
    public function education()
    {
          return $this->hasMany('App\Education');
    }

    // Relationship to the Training and Certification model
    public function training_and_certification()
    {
          return $this->hasMany('App\TrainingAndCertification');
    }

    // Relationship to the Training and Certification model
    public function message()
    {
          return $this->hasMany('App\Message');
    }

    public function due()
    {
          return $this->hasMany('App\Due');
    }


    public function promotion()
    {
          return $this->hasMany('App\Promotions');
    }

    public static function isVerified()
    {
        return $this->verified == User::VERIFIED_USER;
    }


    public static function generateVerificationToken()
    {
        return str_random(40);
    }

     public static function generateMembershipID()
    {
        $prefix = 'NBA';
        $year = Carbon::now()->year;
        $number = (string)mt_rand(1000, 10000);

        $id = $prefix . '/' . $year . '/' . $number;

        return $id;

    }

    public static function getAllUsers()
    {
        $users = User::all();

        return $users;
    }
}
