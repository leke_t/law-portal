<?php

use Faker\Generator as Faker;
use App\User;
use App\Achievement;
use App\Education;
use App\Business;
use App\Contact;
use App\Personal;
use App\TrainingAndCertification;
use App\Message;
/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(User::class, function(\Faker\Generator $faker) {
    static $password;

    return [
        'first_name' => $faker->firstName,
        'last_name' => $faker->lastName,
        'email' => $faker->unique()->safeEmail,
        'password' => $password ?: $password = bcrypt('secret'),
        'membership_id' => User::generateMembershipID(),
        'verified' => $verified = $faker->randomElement([User::VERIFIED_USER, User::UNVERIFIED_USER]),
        'verification_token' => User::generateVerificationToken(),
        'category' => $category = $faker->randomElement([User::BUSINESS_USER, User::REGULAR_USER]),
    ];
});

$factory->define(Personal::class, function (\Faker\Generator $faker) {
  
   return [
        
    ];
});

$factory->define(Contact::class, function (\Faker\Generator $faker) {
  
   return [
      
    ];
});


$factory->define(Education::class, function (\Faker\Generator $faker) {
    $users = User::pluck('id')->toArray();
   return [
        'certificate' => $faker->word,
        'institution' => $faker->paragraph(1),
        'year' => $faker->year,
        'user_id' => $faker->randomElement($users),
    ];
});

$factory->define(TrainingAndCertification::class, function (\Faker\Generator $faker) {
    $users = User::pluck('id')->toArray();
    return [
        'certificate' => $faker->word,
        'institution' => $faker->paragraph(1),
        'year' => $faker->year,
        'user_id' => $faker->randomElement($users),
    ];
});

$factory->define(Achievement::class, function (\Faker\Generator $faker) {
  
   $users = User::pluck('id')->toArray();
   return [
        'project' => $faker->word,
        'year' => $faker->year,
        'user_id' => $faker->randomElement($users),
    ];
});

$factory->define(Business::class, function (\Faker\Generator $faker) {
    $business = User::all()->except('individual')->random();

    return [
        'company_name' => $faker->name,
        'user_id' => $business->id,
        'service' => $faker->paragraph(1),
        'profile' => $faker->paragraph(3),
    ];
});

$factory->define(Message::class, function (\Faker\Generator $faker) {
    $sender = User::all()->random();
    $recipient = User::all()->except($sender->id)->random();

    return [
        'message' => $faker->paragraph(4),
        'sender_id' => $sender->id,
        'recipient_id' => $recipient->id,
        'sender' => $sender->first_name . ' ' . $sender->last_name,
        'recipient' => $recipient->first_name . ' ' . $recipient->last_name,
    ];
});
