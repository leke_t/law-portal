<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Education;
use App\Achievement;
use App\Business;
use App\Personal;
use App\Contact;
use App\TrainingAndCertification;
use App\Message;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        DB::statement("SET FOREIGN_KEY_CHECKS = 0");

        User::truncate();
        Personal::truncate();
        Contact::truncate();
        Education::truncate();
        Achievement::truncate();
        TrainingAndCertification::truncate();
        Business::truncate();

        $usersQuantity = 2000;
        $education = 5000;
        $business = 300;
        $message = 10000;

        //Create user
        factory(User::class, $usersQuantity)->create()->each(function($user){
        	 $user->personal()->save(factory(Personal::class)->make());
        	 $user->contact()->save(factory(Contact::class)->make());
        });

        factory(Education::class, $education)->create();
        
        factory(Achievement::class, $education)->create();

        factory(TrainingAndCertification::class, $education)->create();
       	factory(Business::class, $business)->create();

        factory(Message::class, $message)->create();
    }
}
