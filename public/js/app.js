$(document).ready(function() {
	$('.mobile-menu').click(function() {
		$('.side-menu').slideToggle();
	});

	 // Date Picker
    jQuery('.mydatepicker, #datepicker').datepicker();
    jQuery('#datepicker-autoclose').datepicker({
        autoclose: true,
        todayHighlight: true
    });

    $(".datepickerYr").datepicker( {
        format: " yyyy", // Notice the Extra space at the beginning
        viewMode: "years", 
        minViewMode: "years"
    });
});