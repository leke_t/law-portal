@extends('layouts.master')

@section('title')
    Payment History
@endsection

@section('content') 
  <div class="col-md-9 col col-sm-12 col-xs-12">
            <div class="success-page">
              <center>
                <img src="{{ url('img/error.svg') }}">
                <p>Sorry, there was an error processing your payment</p>
                <a href="{{ route('dashboard') }}" class="btn green-btn">Return to Profile</a>
              </center>

            </div>
          </div>
@endsection


@section('script')
  <script type="text/javascript">
    var id = document.getElementById('payment');
    id.className += " " + "active";
  </script>
@endsection