@extends('layouts.master')

@section('title')
    Payment History
@endsection

@section('content') 
   <div class="col-md-9 col col-sm-12 col-xs-12">
            <div class="membership-table">
              <p class="table-title">Membership Transaction History</p>
              <table class="table members-table"> 
               
                  <thead class="green-table-head"> 
                    <tr> 
                      <th>Year</th> 
                      <th>Transaction Type</th> 
                      <th>Remarks</th> 
                    </tr> 
                  </thead> 
                  <tbody> 
                    @foreach($user['dues'] as $dues)
                    <tr> 
                      <td>{{ $dues->year }}</td> 
                      <td>{{ $dues->purpose}}</td> 
                      @if($dues->paid == '1' || $dues->paid == 1)
                        <td><a href="{{route('receipt.download', ['send' => 'pdf', 'id' => $dues->id])}}">Send Receipt</a> <a href="#">Download elicense</a></td> 
                      @else
                        <td>
                          <form method="POST" action="{{ route('pay') }}" accept-charset="UTF-8" class="form-horizontal" role="form">
                          <input type="hidden" name="email" value="{{$user['email']}}">
                          <input type="hidden" name="amount" value="{{$dues->amount * 100}}">
                         <input type="hidden" name="metadata" value="{{ json_encode($array = ['dues_id' => $dues->id,]) }}" >{{-- For other necessary things you want to add to your payload. it is optional though --}}
                          <input type="hidden" name="reference" value="{{ Paystack::genTranxRef() }}">
                          <input type="hidden" name="key" value="{{ config('paystack.secretKey') }}">
                          {{ csrf_field() }}
                          <button type="submit">Make Payment</button>
                        </form>
                      </td> 
                      @endif
                    </tr> 
                    @endforeach
                  </tbody> 
                </table>
            </div>
          </div>
@endsection


@section('script')
  <script type="text/javascript">
    var id = document.getElementById('payment');
    id.className += " " + "active";
  </script>
@endsection