@extends('layouts.login-master')


@section('title')
  NBA | Create Business Account
@endsection

@section('content')
   @include('layouts.errors')

  <div class="form-box">
    <center>
      <h4>Business Sign Up</h4>
    </center>
    
    <form action="{{ route('business.signup') }}" method="POST">
      {{ csrf_field() }}

      <div class="form-group">
        <label for="exampleInputEmail1">Company Name</label>
        <input type="text" class="form-control" id="exampleInputEmail1" placeholder="Ayo and Co." name="company" value="{{ old('company') }}">
      </div>
      
      <div class="form-group">
        <label for="exampleInputEmail1">Email Address</label>
        <input type="email" class="form-control" id="exampleInputEmail1" placeholder="you@example.com" name="email" value="{{ old('email') }}">
      </div>
      <div class="form-group">
        <label for="exampleInputEmail1">Membership No</label>
        <input type="text" class="form-control" id="exampleInputEmail1" placeholder="Membership No" name="memberid" value="{{old('memberid')}}">
      </div>
      <div class="form-group">
        <label for="exampleInputPassword1">Create Password</label>
        <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password" name="password">
      </div>
                
      <button type="submit" class="btn btn-default green-btn">Sign Up</button>
    </form>
    <center>
    <p>Already have an account? <a href="{{route('user.login')}}">Login</a></p>
    </center>
  </div>
@endsection