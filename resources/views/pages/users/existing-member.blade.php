@extends('layouts.login-master')


@section('title')
  NBA | Existing Member Signup
@endsection

@section('content')
  @if($flash = session('message'))
    <div class="alert alert-success" role="alert">
      {{ $flash }}
    </div>
  @endif
  <div class="form-box">
    <center>
      <h4>Returning Members</h4>
    </center>
    <form action="{{ route('existingMember.signup') }}" method="POST">

      {{ csrf_field() }}
      
      <div class="form-group">
        <label for="exampleInputEmail1">Membership No</label>
          <input type="text" class="form-control" id="exampleInputEmail1" placeholder="Membership No" name="memberid" value="{{ old('memberid')}}">
        </div>

        <button type="submit" class="btn btn-default green-btn">Verify Account</button>
    </form>
    <center>
    <p>Already have an account? <a href="{{route('user.login')}}">Login</a></p>
    </center>
  </div>
@endsection