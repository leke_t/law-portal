@extends('layouts.login-master')


@section('title')
  NBA | Member Login
@endsection

@section('content')
   @if($flash = session('message'))
      @if($alert = session('alert'))
        <div class="alert {{$alert}}" role="alert">
          {{ $flash }}
        </div>
      @endif
  @endif

  <div class="form-box">
    <center>
      <h4>Login</h4>
    </center>
    
    <form name="login" action="{{ route('login') }}" onsubmit="return validateForm();" method="POST">
      {{ csrf_field() }}
      
      <div class="form-group" id="email">
        <label for="exampleInputEmail1">Email / Membership No</label>
        <input type="email" class="form-control" id="exampleInputEmail1" placeholder="Email" name="email" required>
        <span class="error-msg" id="espan"></span>
      </div>
               
      <div class="form-group" id="password">
        <label for="exampleInputPassword1">Password</label>
        <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password" name="password" required>
        <span class="error-msg" id="pspan"></span>
      </div>
      
      <a href="/forgot-password" class="fpwd">Forgot Password</a>
                
      <button type="submit" class="btn btn-default green-btn">Login</button>
              
    </form>

    <a href="{{route('signup')}}" class="cre8">Create Account</a><a href="{{route('business')}}" class="reg-cmp">Register a Business</a>
  </div>
@endsection

@section('script')
  <script type="text/javascript">
//     function validateForm() {
//     var x = document.login.email.value;
//     var y = document.login.password.value;
//     console.log(x);

//     if(x == ""){
//         var email = document.getElementById('email');
//         id.className += " " + " has-error";
//         document.getElementById("espan").innerHTML = "Email field cannot be empty";
//         return false;
//     }

//     if(y == ""){
//        var password  = document.getElementById('password');
//         id.className += " " + " has-error";
//         document.getElementById("pspan").innerHTML = "Password field cannot be empty";
//         return false;
//     }
//     //return( true );
// }
  </script>
@endsection