@extends('layouts.login-master')


@section('title')
  NBA | New Member Signup
@endsection

@section('content')

  @include('layouts.errors')

  <div class="form-box">
    <center>
      <h4>Sign Up</h4>
    </center>

    <form action="{{ route('newMember.signup') }}" method="POST">

      {{ csrf_field() }}

      <div class="form-group">
        <label for="exampleInputEmail1">First Name</label>
        <input type="text" class="form-control" id="exampleInputEmail1" placeholder="John" name="first_name" value="{{old('first_name')}}" required>
      </div>
      <div class="form-group">
        <label for="exampleInputEmail1">Last Name</label>
        <input type="text" class="form-control" id="exampleInputEmail1" placeholder="Doe" name="last_name" value="{{old('last_name')}}" required>
      </div>
      <div class="form-group">
        <label for="exampleInputEmail1">Email</label>
        <input type="email" class="form-control" id="exampleInputEmail1" placeholder="you@example.com" name="email" value="{{old('email')}}" required>
      </div>
      <div class="form-group">
        <label for="exampleInputEmail1">Phone No</label>
        <input type="tel" class="form-control" id="exampleInputEmail1" placeholder="08011122233" name="phone" value="{{old('phone')}}" required>
        <span class="error-msg"></span>
      </div>
      <div class="form-group">
        <label for="exampleInputPassword1">Create Password</label>
        <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password" name="password" required>
      </div>
                
      <button type="submit" class="btn btn-default green-btn">Sign Up</button>
    </form>
    
    <center>
      <p>Already have an account? <a href="{{route('user.login')}}">Login</a></p>
    </center>
  </div>
@endsection

@section('script')
  <script type="text/javascript">
      
  </script>
@endsection