@extends('layouts.login-master')

@section('title')
  NBA | Reset Password
@endsection

@section('content')
  	@if($flash = session('message'))
  	<div class="alert alert-success" role="alert">
        {{$flash}}
  	</div>
  	@endif
    
    <div class="form-box">
    	<center>
         	<h4>Forgot Password</h4>
        </center>
        
        <p>Enter your email address and a password reset link would be sent to you.<br/><br/>
        
        <form action="{{ route('reset.email')}}" method="POST">
        	{{ csrf_field() }}

            <div class="form-group">
            	<label for="exampleInputEmail1">Email Address</label>
                <input type="email" class="form-control" id="exampleInputEmail1" placeholder="Email" name="email" required>
            </div>
                
                
            <button type="submit" class="btn btn-default green-btn">Send Reset Link</button>
        </form>
        
        <center>
            <p><a href="{{ route('user.login') }}">Return to Login</a></p>
        </center>
    </div>
@endsection
