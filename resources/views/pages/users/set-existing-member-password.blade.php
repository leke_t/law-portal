@extends('layouts.login-master')


@section('title')
  NBA | Set Password
@endsection

@section('content')
  @if($flash = session('message'))
    <div class="alert alert-danger" role="alert">
      {{ $flash }}
    </div>
  @endif

  <div class="form-box">
    <center>
      <h4>Set Password</h4>
    </center>
    <form action="{{ route('existingMember.passwordSet') }}" method="POST">


      {{ csrf_field() }}
      <input type="hidden" value="{{$token['token']}}" name="token">
      <div class="form-group">
        <label for="exampleInputEmail1">New Password</label>
        <input type="password" class="form-control" id="exampleInputEmail1" name="password">
      </div>

      <div class="form-group">
        <label for="exampleInputEmail1">Confirm Password</label>
        <input type="password" class="form-control" id="exampleInputEmail1" name="password_confirmation">
      </div>
                
      <button type="submit" class="btn btn-default green-btn">Set Password</button>
    </form>
  </div>
@endsection