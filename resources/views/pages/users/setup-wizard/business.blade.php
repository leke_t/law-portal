@extends('layouts.master')

@section('title')
    NBA | Profile Setup
@endsection

@section('page-header')
  {{-- <div class="col-sm-12">
      <p class="wizard">Step 5 of 5: <span>Achievements</span></p>
  </div> --}}
@endsection

@section('content') 
 <div class="col-md-9 col col-sm-12 col-xs-12">
    <div class=" mb-30">
      <div class="membership-table">
        <p class="table-title">Business Information</p>

        <div class="activate-profile-form">
          <form class="form-horizontal" action="{{ route('business.info') }}" method="POST">


                    {{ csrf_field() }}

                    <input type="hidden" name="user_id" value="{{$user['id']}}">
            <div class="row">
              <div class="col-sm-8">
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-3 control-label">Company Name</label>
                  <div class="col-sm-9">
                    <input type="text" class="form-control" id="inputEmail3" placeholder="" name="company" value="">
                  </div>
                </div>

                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-3 control-label">Services</label>
                  <div class="col-sm-9">
                    <input type="text" class="form-control" id="inputEmail3" placeholder="Services" name="services">
                  </div>
                </div>

                

                <div class="form-group">
                  <label for="inputPassword3" class="col-sm-3 control-label">Business Profile</label>
                  <div class="col-sm-9">
                    <textarea class="form-control" rows="6" placeholder="Compose Message" name="business_profile"></textarea>
                  </div>
                </div>
              </div>

              <div class="col-sm-4">
                <div class="upload-picture">
                  <center>
                  <div class="image-placeholder">

                     <img src="img/camera.png" class="img-responsive">
                  </div>
                  
                  <div class="upload-btn-wrapper">
                      <button class="btn">Upload Company Logo</button>
                      <input type="file" name="myfile" />
                  </div>
                   
                </div>
                </center>
              </div>

              <div class="col-sm-12">
                <div class="form-group">
                  <div class="col-sm-offset-2 col-sm-9">
                    <button type="submit" class="btn btn-default green-btn">Save and Continue</button>
                  </div>
                </div>
              </div>
            </div>
          </form>
        </div>

      </div>
    </div>
  </div>

@endsection

@section('script')
  <script type="text/javascript">
      function populate(s1){
        var s1 = document.getElementById(s1);

        var clone = s1.cloneNode(true);
        $(clone).appendTo($('#achievement'));
        $(clone).find('input').val('');
        
      }
  </script>
@endsection