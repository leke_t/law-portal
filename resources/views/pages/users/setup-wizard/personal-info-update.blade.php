@extends('layouts.master')

@section('title')
    NBA | Profile Setup
@endsection

@section('content') 
  <div class="col-md-9 col col-sm-12 col-xs-12">
    <div class=" mb-30">
      <div class="membership-table">
        <p class="table-title">Personal Information</p>

         <div class="activate-profile-form">
          <form class="form-horizontal" enctype="multipart/form-data" action="{{ route('personal.update') }}" method="POST">
            {{ csrf_field() }}
            <input type="hidden" value="{{$user['id']}}" name="user_id">
              <div class="row">
                <div class="col-sm-8">
                  <div class="form-group">
                    <label for="inputEmail3" class="col-sm-3 control-label">First Name</label>
                      <div class="col-sm-9">
                        <input type="text" class="form-control" id="inputEmail3" placeholder="Email" value="{{$user['first_name']}}" name="first_name">
                      </div>
                  </div>

                  <div class="form-group">
                    <label for="inputEmail3" class="col-sm-3 control-label">Last Name</label>
                        <div class="col-sm-9">
                          <input type="text" class="form-control" id="inputEmail3" placeholder="Email" name="last_name" value="{{$user['last_name']}}">
                        </div>
                  </div>

                  <div class="form-group">
                    <label for="inputPassword3" class="col-sm-3 control-label">Gender</label>
                    <div class="col-sm-9">
                      <select class="form-control" name="gender">
                        <option value="{{$user['gender']}}">{{$user['gender']}}</option>
                        <option value="male">Male</option>
                        <option value="female">Female</option>
                      </select>
                    </div>
                  </div>

                 <div class="form-group">
                    <label for="inputEmail3" class="col-sm-3 control-label ">Date of Birth</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control mydatepicker" id="inputEmail3" placeholder="dd/mm/yyyy" name="dob" value="{{$user['dob']}}">
                    </div>
                  </div>

                  <div class="form-group">
                    <label for="inputPassword3" class="col-sm-3 control-label">State of Origin</label>
                    <div class="col-sm-9">
                      <select class="form-control" name="state" id="state" onchange="populate('state', 'lga')">
                        <option value="{{$user['state']}}">{{$user['state']}}</option>
                        <option value="Abia State">Abia</option>
                        <option value="Adamawa State">Adamawa</option>
                        <option value="Anambra State">Anambra</option>
                        <option value="Akwa Ibom State">Akwa Ibom</option>
                        <option value="Bauchi State">Bauchi</option>
                        <option value="Bayelsa State">Bayelsa</option>
                        <option value="Benue State">Benue</option>
                        <option value="Borno State">Borno</option>
                        <option value="Cross River State">Cross River</option>
                        <option value="Delta State">Delta</option>
                        <option value="Ebonyi State">Ebonyi</option>
                        <option value="Edo State">Edo</option>
                        <option value="Ekiti State">Ekiti</option>
                        <option value="Enugu State">Enugu</option>
                        <option value="Gombe State">Gombe</option>
                        <option value="Imo State">Imo</option>
                        <option value="Jigawa state">Jigawa</option>
                        <option value="Kaduna State">Kaduna</option>
                        <option value="Kano State">Kano</option>
                        <option value="Kastina State">Kastina</option>
                        <option value="Kebbi State">Kebbi</option>
                        <option value="Kogi State">Kogi</option>
                        <option value="Kwara State">Kwara</option>
                        <option value="Lagos State">Lagos</option>
                        <option value="Nasarawa State">Nasarawa</option>
                        <option value="Niger State">Niger</option>
                        <option value="Ogun State">Ogun</option>
                        <option value="Ondo State">Ondo</option>
                        <option value="Osun State">Osun</option>
                        <option value="Oyo State">Oyo</option>
                        <option value="Plateau State">Plateau</option>
                        <option value="Rivers State">Rivers</option>
                        <option value="Sokoto State">Sokoto</option>
                        <option value="Taraba State">Taraba</option>
                        <option value="Yobe State">Yobe</option>
                        <option value="Zamafara State">Zamafara</option>
                        <option value="FCT">FCT Abuja</option>
                      </select>
                    </div>
                  </div>

                  <div class="form-group">
                    <label for="inputPassword3" class="col-sm-3 control-label">Local Govt. Area</label>
                    <div class="col-sm-9">
                      <select class="form-control" name="lga" id="lga">
                        <option value="{{$user['lga']}}">{{$user['lga']}}</option>
                      </select>
                    </div>
                  </div>
                </div>

                <div class="col-sm-4">
                  <div class="upload-picture">
                    <center>
                    <div class="image-placeholder">

                       <img src="img/picture-upload.png" id="default-img" class="img-responsive">
                    </div>
                    
                    <div class="upload-btn-wrapper">
                      
                        <button class="btn">Upload Picture</button>
                        <input type="file" name="avatar" onchange="readURL(this);"/>
                     
                    </div>
                     
                  </div>
                  </center>
                </div>

                <div class="col-sm-12">
                  <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-9">
                      <button type="submit" class="btn btn-default green-btn">Save</button>
                    </div>
                  </div>
                </div>
              </div>
            </form>
          </div>
      </div>
    </div>
  </div>
@endsection

@section('script')
  <script type="text/javascript">
    var id = document.getElementById('profile');
    id.className += " " + "active";
  </script>

  <script type="text/javascript">
    var myData = [];
      $.ajax({
        url: 'https://gist.githubusercontent.com/segebee/7dde9de8e70a207e6e19/raw/90c91f7318d67c9534e3a4d74e4bd755b144e01e/gistfile1.txt',
        dataType: 'json',
        type: 'get',
        cache: false,
        success: function(data){
            myData.push(data);
          }
      });

    function populate(s1, s2){
          var arr = myData[0];

          var s1 = document.getElementById(s1);
          var s2 = document.getElementById(s2);

          s2.innerHTML = "";

          for(var i = 0; i < arr.length; i++)
          {
            if(s1.value == arr[i].state.name){
              var optionArray = [{name: "select"}];
              for(var j = 0; j < arr[i].state.locals.length; j++){
                optionArray.push(arr[i].state.locals[j]);
              }
            }
          }

          $.each(optionArray, function(index, value){
                var newOption = document.createElement("option");
                newOption.value = value.name;
                newOption.innerHTML = value.name;
                s2.options.add(newOption);
               
          });  
    }
       
  </script>

  <script type="text/javascript">
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#default-img')
                    .attr('src', e.target.result)
                    
            };

            reader.readAsDataURL(input.files[0]);
        }
    }
  </script>
@endsection