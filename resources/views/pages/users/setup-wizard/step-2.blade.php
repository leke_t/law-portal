@extends('layouts.master')

@section('title')
    NBA | Profile Setup
@endsection

@section('page-header')
  <div class="col-sm-12">
      <p class="wizard">Step 2 of 5: <span>Contact Information</span></p>
  </div>
@endsection

@section('content') 
     <div class="col-md-9 col col-sm-12 col-xs-12">

            <div class=" mb-30">
              <div class="membership-table">
                <p class="table-title">Contact Information</p>
                
                <div class="activate-profile-form">
                  
                @if($flash = session('message'))
                <div class="alert alert-danger" role="alert">
                  {{ $flash }}
                </div>
                @endif
                  <form class="form-horizontal" action="{{ route('contact.add') }}" method="POST">
                    {{ csrf_field() }}

                    <input type="hidden" value="{{$user['id']}}" name="user_id">
                    <div class="row">
                      <div class="col-sm-12">
                        <div class="form-group">
                          <label for="inputEmail3" class="col-sm-3 control-label">Phone No</label>
                          <div class="col-sm-9">
                            <input type="text" class="form-control" id="inputEmail3" placeholder="09087654321" name="phone" value="{{$user['phone']}}">
                          </div>
                        </div>

                        <div class="form-group">
                          <label for="inputEmail3" class="col-sm-3 control-label">Alt Phone No</label>
                          <div class="col-sm-9">
                            <input type="text" class="form-control" id="inputEmail3" placeholder="08011122233" name="alt_phone">
                          </div>
                        </div>

                        <div class="form-group">
                          <label for="inputPassword3" class="col-sm-3 control-label">Email</label>
                          
                            <div class="col-sm-9">
                              <input type="text" class="form-control" id="inputEmail3" placeholder="" value="{{ $user['email']}}" name="email">
                            </div>
                          
                        </div>

                        <div class="form-group">
                          <label for="inputPassword3" class="col-sm-3 control-label">Alternate Email</label>
                          
                            <div class="col-sm-9">
                              <input type="text" class="form-control" id="inputEmail3" placeholder="you@example.com" name="alt_email">
                            </div>
                          
                        </div>

                       

                        <div class="form-group">
                          <label for="inputPassword3" class="col-sm-3 control-label">Contact Address</label>
                          <div class="col-sm-9">
                            <textarea class="form-control" rows="6" placeholder="Compose Message" name="address"></textarea>
                          </div>
                        </div>
                       
                        <div class="form-group">
                          <div class="col-sm-offset-3 col-sm-9">
                            <button type="submit" class="btn btn-default green-btn">Save and Proceed</button>
                          </div>
                        </div>
                      </div>

                      
                    </div>
                  </form>
                </div>

              </div>
            </div>

            
          </div>
@endsection

@section('script')
  <script type="text/javascript">
    var id = document.getElementById('profile');
    id.className += " " + "active";
  </script>
@endsection