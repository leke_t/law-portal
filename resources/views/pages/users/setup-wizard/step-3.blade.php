@extends('layouts.master')

@section('title')
    NBA | Profile Setup
@endsection

@section('page-header')
  <div class="col-sm-12">
      <p class="wizard">Step 3 of 5: <span>Education</span></p>
  </div>
@endsection

@section('content') 
       <div class="col-md-9 col col-sm-12 col-xs-12">

            <div class=" mb-30">
              <div class="membership-table">
                <p class="table-title">Education Information <span><button><i class="fas fa-plus-circle fa-lg" onclick="populate('clone')"></i></button></span></p>

                <div class="activate-profile-form">
                  <form action="{{ route('education') }}" method="POST">

                    {{ csrf_field() }}

                    <input type="hidden" name="user_id" value="{{$user['id']}}">
                    <div id="education">
                      <div class="row mb-20" id="clone">
                        <div class="col-sm-5">
                          <input type="text" class="form-control" placeholder="Certificate" id="certificate" name="certificate[]"> 
                        </div>
                        <div class="col-sm-5">
                          <input type="text" class="form-control" placeholder="Institutions" id="institution" name="institution[]">
                        </div>
                        <div class="col-sm-2">
                          <input type="text" class="form-control datepickerYr" placeholder="Year" id="year" name="year[]">
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-sm-12">
                        <div class="form-group">
                            <br/>
                            <button type="submit" class="btn btn-default green-btn">Save and Continue</button>
                        </div>
                      </div>
                    </div>
                  </form>
                  {{-- <a href="{{route('set.training')}}"><button class="btn btn-default green-btn" style="float: right;">Skip</button></a> --}}
                </div>

              </div>
            </div>

            
          </div>
@endsection

@section('script')
  <script type="text/javascript">
    var id = document.getElementById('profile');
    id.className += " " + "active";
  </script>

  <script type="text/javascript">
      function populate(s1){
        var s1 = document.getElementById(s1);
        var clone = s1.cloneNode(true);

        if($(s1).find('input').val() == ''){
          alert('Enter the previous field')
        }else if($(clone).find('input').val() == ''){
           alert('Enter the previous field')
        }else{
          $(clone).appendTo($('#education'));
          $(clone).find('input').val('');
        }
      }
  </script>


@endsection