<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>NBA Portal</title>

    <!-- Bootstrap -->
    <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700" rel="stylesheet">
    <link href="{{ url('css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ url('css/fontawesome-all.min.css') }}" rel="stylesheet">
    <link href="{{ url('css/app.css') }}" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
    

    <div class="homepage-bg">
      <div class="container">
        <div class="home-container">
          <div class="row">
            <div class="col-xs-12 col-sm-6 col-md-5">
              <center>
                <img src="{{ url('img/nba-logo.svg') }}">
              </center>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-7">
              <div class="login-form-home">
                  @if($flash = session('message'))
                  <div class="alert alert-danger" role="alert">
                    {{ $flash }}
                  </div>
                  @endif
                <form action="{{ route('login') }}" method="POST">
                  {{ csrf_field() }}
                  <input type="email" placeholder="Email Address" name="email" required>
                  <input type="password" placeholder="Password" name="password" required>
                  <span><a href="/forgot-password" class="fpwd">Forgot Password?</a></span>
                  <input type="submit" value="Login"  name="">
                  <a href="{{ route('signup') }}" class="cre8">Create Account</a><a href="{{ route('business') }}" class="reg-cmp">Register a Business</a>

                  <p>Old or returning members, click here to <a href="/old-members">Sign Up</a> </p>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>


    

    <div class="footer">
      <div class="container">
        <div class="row">
          <div class="col-md-3 col-sm-4 col-xs-12">
            <div class="footer-group">
              <h4>The Association</h4>
              <ul>
                <li><a href="#">About NBA</a></li>
                <li><a href="#">Contact Us</a></li>
                
              </ul>
            </div>
          </div>

           <div class="col-md-3 col-sm-4 col-xs-12">
            <div class="footer-group">
              <h4>How It Works</h4>
              <ul>
                <li><a href="#">Membership Payment</a></li>
                <li><a href="#">Membership History</a></li>
                <li><a href="#">Directory Service</a></li>
                <li><a href="#">Message Center</a></li>
                <li><a href="#">Promote Your Business</a></li>
              </ul>
            </div>
          </div>

          <div class="col-md-2 col-sm-4 col-xs-12">
            <div class="footer-group">
              <h4>Support</h4>
              <ul>
               <li><a href="#">FAQs</a></li>
                <li><a href="#">Feedback & Enquiry</a></li>
              </ul>
            </div>
          </div>

          <div class="col-md-4 col-sm-12 col-xs-12">
            <div class="footer-group">
              <h4>Contact Us</h4>
              <ul>
                <li>
                  <div class="clearfix contact-line">
                    <div class="contact-image"><img src="{{ url('img/dock-phone.png') }}"></div>
                    <div class="contact-text"><p>We are here to help 24/7 <span>07011811877</span></p></div>
                  </div>
                </li>
                <li>
                  <div class="clearfix contact-line">
                    <div class="contact-image"><img src="img/dock-email.png"></div>
                    <div class="contact-text"><p>Support E-mail Address:  <span>help@nbanigeria.gov.ng</span></p></div>
                  </div>
                </li>
              </ul>
                <div class="social-media">
                  <a href="#"><i class="fab fa-lg fa-twitter"></i></a>
                  <a href="#"><i class="fab fa-lg fa-facebook"></i></a>
                  <a href="#"><i class="fab fa-lg fa-instagram"></i></a>
                  <a href="#"><i class="fab fa-lg fa-linkedin"></i></a>
                </div>
              
            </div>
          </div>

        </div>
      </div>
    </div>
    

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="{{ url('js/bootstrap.min.js') }}"></script>
    <script src="{{ url('js/app.js') }}"></script>
  </body>
</html>