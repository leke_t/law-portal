@extends('layouts.master')

@section('title')
    NBA | Compose
@endsection

@section('content') 
   <div class="col-md-9 col col-sm-12 col-xs-12">
            <div class="container-box">
              <div class="tab-group">
                <div class="row">
                  <div class="col-md-12">
                    @if($flash = session('message'))
                      <div class="alert alert-success">
                          {{ $flash }}
                      </div>
                    @endif
                   <h4>Message Center</h4>
                  </div>
                  <div class="col-md-4 col-sm-4 col-xs-12">
                    <a href="#" class="tab-links">Notifications</a>
                  </div>
                  <div class="col-md-4 col-sm-4 col-xs-12">
                    <a href="{{ route('message.inbox' )}}" class="tab-links active">Inbox</a>
                  </div>
                  <div class="col-md-4 col-sm-4 col-xs-12">
                    <a href="{{ route('message.outbox' )}}" class="tab-links">Outbox</a>
                  </div>
                </div>
              </div>

              <div class="row">
                <div class="col-md-12">
                  <div class="search-container">
                    @if(count($messages) > 0)
                      @foreach($messages as $message)
                      <div class="search-result-box">
                        <div class="clearfix">
                          @foreach($user['personals'] as $personal)
                            @if($personal->user_id == $message->sender_id)
                            <div class="search-img"><img src="/uploads/avatars/{{$personal->avatar}}" class="img-responsive"></div>
                            @endif
                          @endforeach
                          <div class="search-text">
                            <h4>{{$message->sender}}</h4>
                            <p>{{strip_tags($message->message)}}</p>
                            <a href="{{route('compose', ['id' => $message->sender_id])}}"><button class="btn primary-btn">Reply</button></a>
                          </div>
                        </div>
                      </div>
                      @endforeach
                      {{ $messages->links() }}
                    @else
                      <div class="empty-mail">
                        <center>
                          <img src="{{ url('img/empty-inbox.svg') }}"><br/><br/>
                           <p>No message here</p>
                        </center>
                     </div>
                    @endif
                  </div>
                </div>
              </div>
            </div>
          </div>
@endsection


@section('script')
  <script type="text/javascript">
    var id = document.getElementById('message');
    id.className += " " + "active";
  </script>
@endsection