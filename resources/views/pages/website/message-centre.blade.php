@extends('layouts.master')

@section('title')
    NBA | Compose
@endsection

@section('styles')
  <script src="{{asset('js/tinymce/jquery.tinymce.min.js')}}"></script>
  <script src="{{asset('js/tinymce/tinymce.min.js')}}"></script>
@endsection

@section('content') 
   <div class="col-md-9 col col-sm-12 col-xs-12">
            <div class="container-box">
              <div class="tab-group">
                <div class="row">
                  <div class="col-md-12">
                   <h4>Message Center</h4>
                  </div>
                  <div class="col-md-4 col-sm-4 col-xs-12">
                    <a href="#" class="tab-links">Notifications</a>
                  </div>
                  <div class="col-md-4 col-sm-4 col-xs-12">
                    <a href="{{ route('message.inbox' )}}" class="tab-links">Inbox</a>
                  </div>
                  <div class="col-md-4 col-sm-4 col-xs-12">
                    <a href="{{ route('message.send') }}" class="tab-links active">Send Message</a>
                  </div>
                </div>
              </div>

              <div class="row">
                <div class="col-md-12">
                  <div class="search-container">
                    <div class="search-result-box">
                      <div class="clearfix message-area">
                       
                        <!-- <div class="search-img"><img src="img/calendar-planner.svg" class="img-responsive"></div> -->
                        <div class="tabbed-group">
                          
                          <form method="POST" action="{{ route('message.send') }}">
                            {{ csrf_field() }}
                            <input type="hidden" name="recipient" value="{{$recipient['id']}}">
                            {{--  <div class="name-search">
                              <input type="text" class="form-control" placeholder="Message Subject">
                            </div> --}}
                            <textarea class="form-control editor" rows="6" placeholder="Compose Message" name="message"></textarea>
                            <input type="Submit" value="Send Message">

                          </form>
                        </div>
                      </div>
                    </div>

                  </div>
                </div>
              </div>
            </div>
          </div>
@endsection

@section('script')
  <script type="text/javascript">
    var id = document.getElementById('message');
    id.className += " " + "active";
  </script>

  <script type="text/javascript">
      var editor_config = {
    path_absolute : "/",
    selector: "textarea.editor",
    plugins: [
      "advlist autolink lists link image charmap print preview hr anchor pagebreak",
      "searchreplace wordcount visualblocks visualchars code fullscreen",
      "insertdatetime media nonbreaking save table contextmenu directionality",
      "emoticons template paste textcolor colorpicker textpattern codesample",
      "fullpage toc spellchecker imagetools help"
    ],
    toolbar: "insertfile undo redo | styleselect | bold italic strikethrough | alignleft aligncenter alignright alignjustify | ltr rtl | bullist numlist outdent indent removeformat formatselect| link image media | emoticons charmap | code codesample | forecolor backcolor",
    external_plugins: { "nanospell": "{{url('js/tinymce/plugins/nanospell/plugin.js')}}" },
    nanospell_server:"php",
    menubar:false,
    browser_spellcheck: true,
    relative_urls: false,
    remove_script_host: false,
    file_browser_callback : function(field_name, url, type, win) {
      var x = window.innerWidth || document.documentElement.clientWidth || document.getElementsByTagName('body')[0].clientWidth;
      var y = window.innerHeight|| document.documentElement.clientHeight|| document.getElementsByTagName('body')[0].clientHeight;

      var cmsURL = editor_config.path_absolute + 'laravel-filemanager?field_name=' + field_name;
      if (type == 'image') {
        cmsURL = cmsURL + "&type=Images";
      } else {
        cmsURL = cmsURL + "&type=Files";
      }

      tinymce.activeEditor.windowManager.open({
        file: "{{ route('elfinder.tinymce4') }}", // use an absolute path!
        title: 'File manager',
        width: 900,
        height: 450,
        resizable: 'yes'
      }, {
        setUrl: function (url) {
          win.document.getElementById(field_name).value = url;
        }
      });
    }
  };

  tinymce.init(editor_config);
  </script>

  <script>
    {!! \File::get(base_path('vendor/barryvdh/laravel-elfinder/resources/assets/js/standalonepopup.min.js')) !!}
  </script>
@endsection