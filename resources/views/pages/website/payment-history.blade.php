@extends('layouts.master')

@section('title')
    NBA | Profile Summary
@endsection

@section('content') 
  <div class="col-md-9 col col-sm-12 col-xs-12">
            <div class="membership-table">
              <p class="table-title">Membership Transaction History</p>
              <table class="table members-table"> 
               
                  <thead class="green-table-head"> 
                    <tr> 
                      <th>Year</th> 
                      <th>Transaction Type</th> 
                      <th>Remarks</th> 
                    </tr> 
                  </thead> 
                  <tbody> 
                    <tr> 
                      <td>2017</td> 
                      <td>Payment for 2017 Annual Membership Fee Payment</td> 
                      <td><a href="#">Download Receipt</a> <a href="#">Download elicense</a></td> 
                    </tr> 
                    <tr> 
                      <td>2017</td> 
                      <td>Payment for 2017 Annual Membership Fee Payment</td> 
                      <td><a href="#">Download Receipt</a> <a href="#">Download elicense</a></td> 
                    </tr>
                    <tr> 
                      <td>2017</td> 
                      <td>Payment for 2017 Annual Membership Fee Payment</td> 
                      <td><a href="#">Download Receipt</a> <a href="#">Download elicense</a></td> 
                    </tr>
                    <tr> 
                      <td>2017</td> 
                      <td>Payment for 2017 Annual Membership Fee Payment</td> 
                      <td><a href="#">Download Receipt</a> <a href="#">Download elicense</a></td> 
                    </tr> 
                    <tr> 
                      <td>2017</td> 
                      <td>Payment for 2017 Annual Membership Fee Payment</td> 
                      <td><a href="#">Download Receipt</a> <a href="#">Download elicense</a></td> 
                    </tr>
                    <tr> 
                      <td>2017</td> 
                      <td>Payment for 2017 Annual Membership Fee Payment</td> 
                      <td><a href="#">Download Receipt</a> <a href="#">Download elicense</a></td> 
                    </tr>
                  </tbody> 
                </table>
            </div>
          </div>
@endsection

@section('script')
  <script type="text/javascript">
    var id = document.getElementById('history');
    id.className += " " + "active";
  </script>
@endsection