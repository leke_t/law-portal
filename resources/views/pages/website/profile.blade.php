@extends('layouts.master')

@section('title')
    NBA | Profile Summary
@endsection

@section('content') 
  <div class="col-md-9 col col-sm-12 col-xs-12">

            <div class=" mb-30">
              <div class="membership-table">
                <p class="table-title"><b>Personal Details</b> <span><a href="{{route('personal.edit')}}"><i class="fas fa-pen-square"></i> Edit</a></span></p>

                <div class="personal-section clearfix">
                  <p class="text-label">Full Name <span>{{$user['fullname']}}</span></p>

                  @if(isset($user['gender']))
                    <p class="text-label">Gender<span>{{ ucwords($user['gender']) }}</span></p>
                  @else
                    <p class="text-label">Gender<span>N/A</span></p>
                  @endif

                  @if(isset($user['dob']))
                    <p class="text-label">Date of Birth<span>{{$user['dob']}}</span></p>
                  @else
                    <p class="text-label">Date of Birth<span>N/A</span></p>
                  @endif

                  @if(isset($user['state']))
                    <p class="text-label">State of Origin <span>{{ $user['state'] }}</span></p>
                    <p class="text-label">Local Govt. Area <span>{{ $user['lga'] }}</span></p>
                  @else
                    <p class="text-label">State of Origin <span>N/A</span></p>
                    <p class="text-label">Local Govt. Area <span>N/A</span></p>
                  @endif
                </div>
              </div>
            </div>

            <div class=" mb-30">
              <div class="membership-table">
                <p class="table-title"><b>Contact Details</b> <span><a href="{{route('contact.edit')}}"><i class="fas fa-pen-square"></i> Edit</a></span></p>

                <div class="personal-section clearfix">
                  <p class="text-label">Phone No <span>{{$user['phone']}}</span></p>
                  <p class="text-label">Email <span>{{$user['email']}}</span></p>
                  <p class="text-label">Address<span>{{ $user['address'] }}</span></p>
                </div>
              </div>
            </div>

            <div class=" mb-30">
              <div class="membership-table">
                @if(count($user['education']) > 0)
                  <p class="table-title">Education <span><a href="{{route('edit.education')}}"><i class="fas fa-pen-square"></i> Edit</a></span></p>
                  @foreach($user['education'] as $education)
                    <div class="section-ctn clearfix">
                      <div class="section-icon">
                        <img src="img/school.svg" class="img-responsive">
                      </div>
                      <div class="section-text">
                        <p><span class="certificate">{{$education->certificate}}</span>
                          <span class="school">{{$education->institution}}</span>
                          <span class="year">{{$education->year}}</span>
                        </p>
                      </div>
                    </div>
                  @endforeach
                @else
                      <p class="table-title"><b>Education</b></p>

                      <div class="section-ctn empty-section clearfix">
                        <center>
                          <img src="img/school.svg" class="img-responsive">
                          <p>No Education Added</p>
                          <a href="{{route('edit.education', ['add' => 'add new'])}}"><button class="btn primary-btn green-btn">Add Education</button></a>
                        </center>
                      </div>
                @endif
              </div>
            </div>

            <div class=" mb-30">
              <div class="membership-table">
                @if(count($user['training']) > 0)
                   <p class="table-title"><b>Training & Certificates</b> <span><a href="{{route('edit.training')}}"><i class="fas fa-pen-square"></i> Edit</a></span></p>
                  @foreach($user['training'] as $training)
                    <div class="section-ctn clearfix">
                      <div class="section-icon">
                        <img src="img/diploma.svg" class="img-responsive">
                      </div>
                      <div class="section-text">
                        <p><span class="certificate">{{$training->certificate}}</span>
                          <span class="school">{{$training->institution}}</span>
                          <span class="year">{{$training->year}}</span>
                        </p>
                      </div>
                    </div>
                  @endforeach
                @else
                      <p class="table-title"><b>Training & Certificates</b></p>

                      <div class="section-ctn empty-section clearfix">
                        <center>
                          <img src="img/diploma.svg" class="img-responsive">
                          <p>No Training or Certifications</p>
                          <a href="{{ route('edit.training', ['add' => 'add new'])}}"><button class="btn primary-btn green-btn">Add Training</button></a>
                        </center>
                      </div>
                @endif

              </div>
            </div>

            <div class=" mb-30">
              <div class="membership-table">
                @if(count($user['achievement']) > 0)
                <p class="table-title"><b>Achievements</b> <span><a href="{{route('edit.achievement')}}"><i class="fas fa-pen-square"></i> Edit</a></span></p>
                  @foreach($user['achievement'] as $achievement)
                    <div class="section-ctn clearfix">
                      <div class="section-icon">
                        <img src="img/medal.svg" class="img-responsive">
                      </div>
                      <div class="section-text">
                        <p><span class="certificate">{{$achievement->project}}</span>
                          <span class="year">{{$achievement->year}}</span>
                        </p>
                      </div>
                    </div>
                  @endforeach
                @else
                      <p class="table-title"><b>Achievements</b></p>

                      <div class="section-ctn empty-section clearfix">
                        <center>
                          <img src="img/medal.svg" class="img-responsive">
                          <p>No Achievements Added</p>
                          <a href="{{route('edit.achievement', ['add' => 'add new'])}}"><button class="btn primary-btn green-btn">Add Achievement</button></a>
                        </center>
                      </div>
                @endif


              </div>
            </div>
            
          </div>
@endsection

@section('script')
  <script type="text/javascript">
    var id = document.getElementById('profile');
    id.className += " " + "active";
  </script>
@endsection