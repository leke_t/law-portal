 @extends('layouts.master')

@section('title')
    NBA | Promote your business
@endsection

@section('content') 
  <div class="col-md-9 col col-sm-12 col-xs-12">
            <div class=" mb-30">
              <div class="membership-table">
                @if($flash = session('message'))
                          <div class="alert alert-success" role="alert">
                            {{ $flash }}
                          </div>
                    @endif
                <p class="table-title">Promote Your Business</p>

                <div class="activate-profile-form">
                  <form class="form-horizontal" method="POST" action="{{route('promotion.send')}}">

                    {{ csrf_field()}}
                    <div class="row">
                      <div class="col-sm-12">
                        <div class="form-group">
                          <label for="inputEmail3" class="col-sm-3 control-label">Company Name</label>
                          <div class="col-sm-9">
                            <input type="text" class="form-control" id="inputEmail3" placeholder="Email" name="company_name" required>
                          </div>
                        </div>

                        <div class="form-group">
                          <label for="inputEmail3" class="col-sm-3 control-label">Services</label>
                          <div class="col-sm-9">
                            <label class="checkbox-inline">
                              <input type="checkbox" id="inlineCheckbox1" value="Ad Placement" name="check[]"> Ad Placement
                            </label>
                            <label class="checkbox-inline">
                              <input type="checkbox" id="inlineCheckbox2" value="Email Marketing" name="check[]"> Email Marketing
                            </label>
                            <label class="checkbox-inline">
                              <input type="checkbox" id="inlineCheckbox3" value="SMS Marketing" name="check[]"> SMS Marketing
                            </label>
                            <label class="checkbox-inline">
                              <input type="checkbox" id="inlineCheckbox3" value="Directory Search" name="check[]"> Directory Search
                            </label>
                          </div>
                        </div>

                        

                        <div class="form-group">
                          <label for="inputPassword3" class="col-sm-3 control-label">Request Description</label>
                          <div class="col-sm-9">
                            <textarea class="form-control" rows="6" placeholder="Compose Message" name="description" required></textarea>
                          </div>
                        </div>
                      </div>

                      

                      <div class="col-sm-12">
                        <div class="form-group">
                          <div class="col-sm-offset-3 col-sm-9">
                            <button type="submit" class="btn btn-default green-btn">Send Message</button>
                          </div>
                        </div>
                      </div>
                    </div>
                  </form>
                </div>

              </div>
            </div>

          
          </div>
@endsection

@section('script')
  <script type="text/javascript">
    var id = document.getElementById('profile');
    id.className += " " + "active";
  </script>
@endsection