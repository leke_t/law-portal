@extends('layouts.master')

@section('title')
    NBA | Directory Service
@endsection

@section('content')
	 <div class="col-md-9 col col-sm-12 col-xs-12">
            <div class="container-box">
              <form id="search" method="POST" action="{{ route('directory.result') }}">
                {{ csrf_field() }}
                <input type="text"  class="form-control" placeholder="Search for people and businesses" name="query">
              </form>

              @if(!isset($data['query']))
              <div class="search-count">
                <p>Search Results for #...................</p>
              </div>
              @else
              <div class="search-count">
                <p>{{$data['count']}} result found for: {{ucwords($data['query'])}}</p>
              </div>
              @endif

              <div class="row">
                <div class="col-md-12">
                  <div class="search-container">
                    @if(!isset($data))
                      <div class="empty-mail">
                        <center>
                          <img src="{{ url('img/search.svg') }}"><br/><br/>
                           <p></p>
                        </center>
                     </div>
                    @else
                      @if(!isset($data['users']) || !isset($data['business']))
                         <div class="empty-mail">
                           <center>
                          <img src="{{ url('img/search.svg') }}"><br/><br/>
                           <p>No result found</p>
                          </center>
                        </div>
                      @else
                        @foreach($data['users'] as $users)
                          @if(!isset($users))
                            <p>
                            </p>
                          @else
                            <a href="{{route('show-user.profile', ['id' => $users->id])}}">
                              <div class="search-result-box">
                                <div class="clearfix">
                                  @foreach($data['personals'] as $personal)
                                    @if($users->id == $personal->user_id)
                                    <div class="search-img"><img src="/uploads/avatars/{{$personal->avatar}}" class="img-responsive"></div>
                                    @endif
                                  @endforeach
                                  <div class="search-text">
                                    <h4>{{$users->first_name . ' ' . $users->last_name}}</h4>
                                    @foreach($data['education'] as $education)
                                        @if($education == null)
                                          <p></p>
                                        @else
                                          <p>{{$education->certificate}}</p>
                                          <p>{{$education->institution}}</p>
                                          @break
                                        @endif
                                    @endforeach
                                  </div>
                                </div>
                              </div>
                            </a>
                          @endif
                        @endforeach

                        @foreach($data['business'] as $business)
                          @if(!isset($business))
                            <p></p>
                          @else
                            <a href="{{route('show-business.profile', ['id' => $business->id])}}">
                            <div class="search-result-box">
                              <div class="clearfix">
                                <div class="search-img"><img src="/uploads/avatars/{{$business->avatar}}" class="img-responsive"></div>
                                <div class="search-text">
                                  <h4>{{$business->company_name}}</h4>
                                  <p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage.</p>
                                 </div>
                                </div>
                              </div>
                            </a>
                          @endif
                      @endforeach
                      @endif
                    @endif
                  </div>
                </div>
              </div>
            </div>
          </div>
@endsection

@section('script')
  <script>
    document.getElementById('body').onkeyup = function(e) {
      if (e.keyCode === 13) {
        document.getElementById('search').submit(); // your form has an id="form"
      }
        return true;
     }
</script>

  <script type="text/javascript">
    var id = document.getElementById('directory');
    id.className += " " + "active";
  </script>

@endsection