@extends('layouts.master')

@section('title')
    NBA | Profile Summary
@endsection

@section('content') 
  <div class="col-md-9 col col-sm-12 col-xs-12">

            <div class=" mb-30">
              <div class="membership-table">
                 <p class="table-title"><b>Business Details</b> <span><a href="#"><button class="btn primary-btn">Send Message</button></a></span></p>

                <div class="personal-section clearfix">
                  <p class="text-label">Phone No <span>08065438987</span></p>
                  <p class="text-label">Email <span>neo.anderson@matrix.com</span></p>
                  <p class="text-label">Address<span>Block 10, Flat 13, 1004 Estate, Victoria Island, Lagos.</span></p>
                </div>
              </div>
            </div>

            <div class=" mb-30">
              <div class="membership-table">
                @if(count($user['achievement']) > 0)
                <p class="table-title"><b>Achievements</b> <span><a href="{{route('edit.achievement')}}"><i class="fas fa-pen-square"></i> Edit</a></span></p>
                  @foreach($user['achievement'] as $achievement)
                    <div class="section-ctn clearfix">
                      <div class="section-icon">
                        <img src="img/medal.svg" class="img-responsive">
                      </div>
                      <div class="section-text">
                        <p><span class="certificate">{{$achievement->project}}</span>
                          <span class="year">{{$achievement->year}}</span>
                        </p>
                      </div>
                    </div>
                  @endforeach
                @else
                      <p class="table-title"><b>Achievements</b></p>

                      <div class="section-ctn empty-section clearfix">
                        <center>
                          <img src="img/medal.svg" class="img-responsive">
                          <p>No Achievements Added</p>
                          <a href="{{route('edit.achievement', ['add' => 'add new'])}}"><button class="btn primary-btn green-btn">Add Achievement</button></a>
                        </center>
                      </div>
                @endif


              </div>
            </div>
            
          </div>
@endsection

@section('script')
  <script type="text/javascript">
    var id = document.getElementById('profile');
    id.className += " " + "active";
  </script>
@endsection