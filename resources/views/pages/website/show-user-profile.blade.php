@extends('layouts.master')

@section('title')
    NBA | Profile Summary
@endsection

@section('content') 
  <div class="col-md-9 col col-sm-12 col-xs-12">

            <div class=" mb-30">
              <div class="membership-table">
                <p class="table-title"><b>Personal Details</b> <span><a href="{{route('compose', ['id' => $user['id']])}}"><button class="btn primary-btn">Send Message</button></a></span></p>

                <div class="personal-section clearfix">
                  <p class="text-label">Full Name <span>{{$user['name']}}</span></p>
                  <p class="text-label">Gender Name <span>{{ ucwords($user['gender']) }}</span></p>
                  <p class="text-label">Date of Birth Name <span>{{$user['dob']}}</span></p>
                  <p class="text-label">State of Origin <span>{{ $user['state'] }}</span></p>
                  <p class="text-label">Local Govt. Area <span>{{ $user['lga'] }}</span></p>
                </div>
              </div>
            </div>

            <div class=" mb-30">
              <div class="membership-table">
                <p class="table-title"><b>Contact Details</b></p>

                <div class="personal-section clearfix">
                  <p class="text-label">Phone No <span>{{$user['phone']}}</span></p>
                  <p class="text-label">Email <span>{{$user['email']}}</span></p>
                  <p class="text-label">Address<span>{{ $user['address'] }}</span></p>
                </div>
              </div>
            </div>

            @if(count($user['education']) > 0)
            <div class=" mb-30">
              <div class="membership-table">
                <p class="table-title"><b>Education</b></p>
                  @foreach($user['education'] as $education)
                    <div class="section-ctn clearfix">
                      <div class="section-icon">
                        <img src="{{url('img/school.svg')}}" class="img-responsive">
                      </div>
                      <div class="section-text">
                        <p><span class="certificate">{{ucwords($education->certificate)}}</span>
                          <span class="school">{{ucwords($education->institution)}}</span>
                          <span class="year">{{$education->year}}</span>
                        </p>
                      </div>
                    </div>
                  @endforeach
              </div>
            </div>
            @endif

            @if(count($user['training']) > 0)
            <div class=" mb-30">
              <div class="membership-table">
                <p class="table-title"><b>Training & Certificates</b></p>
                  @foreach($user['training'] as $training)
                    <div class="section-ctn clearfix">
                      <div class="section-icon">
                        <img src="{{url('img/school.svg')}}" class="img-responsive">
                      </div>
                      <div class="section-text">
                        <p><span class="certificate">{{ucwords($training->certificate)}}</span>
                          <span class="school">{{ucwords($training->institution)}}</span>
                          <span class="year">{{$training->year}}</span>
                        </p>
                      </div>
                    </div>
                  @endforeach
              </div>
            </div>
            @endif

            @if(count($user['achievement']) > 0)
            <div class=" mb-30">
              <div class="membership-table">
                <p class="table-title"><b>Achievements</b></p>
                  @foreach($user['achievement'] as $achievement)
                    <div class="section-ctn clearfix">
                      <div class="section-icon">
                        <img src="{{url('img/school.svg')}}" class="img-responsive">
                      </div>
                      <div class="section-text">
                        <p><span class="certificate">{{ucwords($achievement->project)}}</span>
                          <span class="year">{{$achievement->year}}</span>
                        </p>
                      </div>
                    </div>
                  @endforeach
              </div>
            </div>
            @endif
            
          </div>
@endsection

@section('script')
  <script type="text/javascript">
    var id = document.getElementById('profile');
    id.className += " " + "active";
  </script>
@endsection