 <div class="footer">
      <div class="container">
        <div class="row">
          <div class="col-md-3 col-sm-4 col-xs-12">
            <div class="footer-group">
              <h4>The Association</h4>
              <ul>
                <li><a href="#">About NBA</a></li>
                <li><a href="#">Contact Us</a></li>
                
              </ul>
            </div>
          </div>

           <div class="col-md-3 col-sm-4 col-xs-12">
            <div class="footer-group">
              <h4>How It Works</h4>
              <ul>
                <li><a href="#">Membership Payment</a></li>
                <li><a href="#">Membership History</a></li>
                <li><a href="#">Directory Service</a></li>
                <li><a href="#">Message Center</a></li>
                <li><a href="#">Promote Your Business</a></li>
              </ul>
            </div>
          </div>

          <div class="col-md-2 col-sm-4 col-xs-12">
            <div class="footer-group">
              <h4>Support</h4>
              <ul>
               <li><a href="#">FAQs</a></li>
                <li><a href="#">Feedback & Enquiry</a></li>
              </ul>
            </div>
          </div> 

          <div class="col-md-4 col-sm-12 col-xs-12">
            <div class="footer-group">
              <h4>Contact Us</h4>
              <ul>
                <li>
                  <div class="clearfix contact-line">
                    <div class="contact-image"><img src="{{ url('img/dock-phone.png') }}"></div>
                    <div class="contact-text"><p>We are here to help 24/7 <span>07011811877</span></p></div>
                  </div>
                </li>
                <li>
                  <div class="clearfix contact-line">
                    <div class="contact-image"><img src="{{ url('img/dock-email.png') }}"></div>
                    <div class="contact-text"><p>Support E-mail Address:  <span>help@nbanigeria.gov.ng</span></p></div>
                  </div>
                </li>
              </ul>
                <div class="social-media">
                  <a href="#"><i class="fab fa-lg fa-twitter"></i></a>
                  <a href="#"><i class="fab fa-lg fa-facebook"></i></a>
                  <a href="#"><i class="fab fa-lg fa-instagram"></i></a>
                  <a href="#"><i class="fab fa-lg fa-linkedin"></i></a>
                </div>
              
            </div>
          </div>

        </div>
      </div>
    </div>