<div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="top-bar">
            <ul>
              @if(Auth::check())
                <li><a href="#">{{$user['fullname']}}<span><img src="/uploads/avatars/{{$user['avatar']}}"></span></a></li>
                <li><a href="#">Support</a></li>
                <li><a href="{{route('logout')}}">Logout</a></li>
              @else
                  <li><a href="#">Unregistered member<span><img src="{{ url('img/user-icon.png') }}"></span></a></li>
                  <li><a href="#">Support</a></li>
              @endif
            </ul>
          </div>
        </div>
      </div>
    </div>

  <div class="logo-bg">
      <div class="container">
        <div class="row">
          <div class="col-md-6 col-sm-12 col-xs-12">
            <div class="logo-img">
              <img src="{{ url('img/logo@2x.png')}}" class="img-responsive" width="531" height="107">
            </div>
          </div>
        </div>
      </div>
    </div>