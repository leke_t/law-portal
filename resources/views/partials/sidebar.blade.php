<div class="col-md-3 col col-sm-12 col-xs-12">
            <div class="all-menu">
              <div class="mobile-menu"><i class="fas fa-bars"></i> &nbsp;&nbsp;Menu</div>
              <div class="side-menu">
                <ul>
                  <li class="" id="profile"><a href="/home">My Profile</a></li>
                  <li id="payment"><a href="{{route('dues.history')}}">Membership Payment</a></li>
                  {{-- <li id="history"><a href="#">Membership History</a></li> --}}
                  <li id="directory"><a href="{{route('directory')}}">Directory Service</a></li>
                  <li id="message"><a href="{{ route('message.inbox') }}">Message Center</a></li>
                </ul>
              </div>
            </div>

            <div class="promote-business">
              <p class="green-txt">Promote</p>
              <h2>Your Business</h2>
              <p>Business Listing  |  Ad Services   |  Marketing Communications</p>
              <a href="{{ route('promotion.get')}}">Start Today</a>
            </div>
          </div>