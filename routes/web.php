<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['middleware' => 'guest'], function(){

	/*
	*	Home page
	*/
	Route::get('/', 'User\UserController@index');


	/*
	* Signup Routes for all possible ways a user or business can signup
	*/

	Route::get('/business', 'User\UserController@business')->name('business');
	Route::get('/old-members', 'User\UserController@existingMember');
	Route::get('/signup', 'User\UserController@newMember')->name('signup');

	Route::post('/business-register', 'User\UserController@newMemberStore')->name('newMember.signup');
	Route::post('/new-member-register', 'User\UserController@businessStore')->name('business.signup');
	Route::post('/exiting-member-register', 'User\UserController@existingMemberStore')->name('existingMember.signup');

	/*
	*	Setting password for an existing member is who signing up
	*/
	Route::get('/set-password/{token}', 'User\UserController@setExistingMemberPassword');
	Route::post('/set-password', 'User\UserController@setPassword')->name('existingMember.passwordSet');

	/*
	*	Login Route
	*/
	Route::get('/login', 'User\UserController@loginView')->name('user.login');
	Route::post('/login', 'User\UserController@login')->name('login');


	/*
	*	Password Reset route
	*/
	Route::get('forgot-password', 'User\UserController@passwordResetView')->name('get.passwordResetViewWithoutToken');
	Route::get('forgot-password/{token}', 'User\UserController@passwordResetView')->name('get.passwordResetView');

	Route::post('send-reset-email', 'User\UserController@sendResetEmail')->name('reset.email');
	Route::post('reset', 'User\UserController@resetPassword')->name('password.reset');

});


Route::group(['middleware' => 'auth'], function(){
	Route::get('logout', 'User\UserController@logout')->name('logout');

	Route::get('personal/set', 'Website\WebsiteController@setPersonalInformation')->name('personal.info');
	Route::get('personal/edit', 'Website\WebsiteController@updatePersonalInformation')->name('personal.edit');
	Route::post('personal/store', 'Website\WebsiteController@personalInformation')->name('personal');
	Route::post('personal/update', 'Website\WebsiteController@updatePersonalInformationStore')->name('personal.update');

	Route::get('contact/set', 'Website\WebsiteController@setContactInformation')->name('contact.info');
	Route::get('contact/edit', 'Website\WebsiteController@updateContactInformation')->name('contact.edit');
	Route::post('contact/store', 'Website\WebsiteController@contactInformation')->name('contact.add');
	Route::post('contact/update', 'Website\WebsiteController@updateContactInformationStore')->name('contact.update');

	Route::get('education/set', 'Website\WebsiteController@setEducation')->name('set.education');
	Route::get('education/edit', 'Website\WebsiteController@updateEducation')->name('edit.education');
	Route::post('education/store', 'Website\WebsiteController@education')->name('education');
	Route::post('education/update', 'Website\WebsiteController@updateEducationStore')->name('education.update');

	Route::get('training/set', 'Website\WebsiteController@setTrainingAndCertifications')->name('set.training');
	Route::post('training/store', 'Website\WebsiteController@trainingAndCertifications')->name('training');
	Route::get('training/edit', 'Website\WebsiteController@updateTrainingAndCertifications')->name('edit.training');
	Route::post('training/update', 'Website\WebsiteController@updateTrainingAndCertificationsStore')->name('training.update');

	Route::get('achievements/set', 'Website\WebsiteController@setAchievements')->name('set.achievement');
	Route::post('achievements/store', 'Website\WebsiteController@achievements')->name('achievement');
	Route::get('achievements/edit', 'Website\WebsiteController@updateAchievements')->name('edit.achievement');
	Route::post('achievements/update', 'Website\WebsiteController@updateAchievementsStore')->name('achievement.update');

	Route::get('business-info', 'Website\WebsiteController@setBusinessInformation')->name('set.business');
	Route::post('business-info', 'Website\WebsiteController@businessInformation')->name('business.info');
	
	
	Route::get('/home', 'Website\WebsiteController@index')->name('dashboard');

	Route::get('/directory-service', 'Website\WebsiteController@directory')->name('directory');

	Route::post('/directory-service-result', 'Website\WebsiteController@directoryResult')->name('directory.result');

	Route::get('/user/profile/{id}', 'Website\WebsiteController@showUserProfile')->name('show-user.profile');
	Route::get('/business/profile/{id}', 'Website\WebsiteController@showBusinessProfile')->name('show-business.profile');


	/*
	* Messaging Routes
	*/
	Route::get('/message/compose/{id}', 'Message\MessageController@getComposeBox')->name('compose');
	Route::post('/message/send', 'Message\MessageController@sendMessage')->name('message.send');
	Route::get('/message/inbox', 'Message\MessageController@inbox')->name('message.inbox');
	Route::get('/message/outbox', 'Message\MessageController@outbox')->name('message.outbox');

	/*
	* Payment Routes
	*/
	Route::get('/payment/history', 'Dues\DuesController@index')->name('dues.history');
	Route::post('/pay', 'Dues\DuesController@redirectToGateway')->name('pay');
	Route::get('/payment/callback', 'Dues\DuesController@handleGatewayCallback');
	Route::get('/payment/success', 'Dues\DuesController@paymentSuccessPage')->name('payment.success');
	Route::get('/payment/error', 'Dues\DuesController@paymentSuccessPage')->name('payment.error');
	Route::get('/payment/receipt', 'Dues\DuesController@receipt')->name('receipt.download');

	Route::get('/promotion/start', 'Promotion\PromotionController@index')->name('promotion.get');
	Route::post('/promotion/send', 'Promotion\PromotionController@promote')->name('promotion.send');

});
